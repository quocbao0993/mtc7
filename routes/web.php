<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('homepage.homepage');
});
Route::get('/about', function () {
    return view('homepage.about');
});
// Route::get('/test', function () {
//     return view('test');
// });
Route::get('/contact', function () {
    return view('homepage.contact');
});
Route::get('/catalogue', function () {
    return view('homepage.catalogue');
});
Route::get('/tin-tuc', function () {
    return view('homepage.tintuc');
});
Route::get('/tu-van', function () {
    return view('homepage.tuvan');
});
Route::get('/san-pham', function () {
    return view('products.sanpham');
});
Route::get('/o-lan-con', function () {
    return view('products.olancon');
});
Route::get('/pk-vong-bi', function () {
    return view('products.pkvongbi');
});
Route::get('/sp-chuyen-biet', function () {
    return view('products.spchuyenbiet');
});
Route::get('/o-lan-thanh-ray', function () {
    return view('products.olanthanhray');
});
Route::get('/cum-goi-doi-o-lan', function () {
    return view('products.cumgoidovaolan');
});
Route::get('/vong-bi-chinh-xac', function () {
    return view('products.vongbichinhxac');
});
Route::get('/o-vanh-xoay', function () {
    return view('products.ovanhxoay');
});
Route::get('/o-truot', function () {
    return view('products.otruot');
});
Route::get('/phot-chan-dau', function () {
    return view('products.photchandau');
});
Route::get('/phot-truyen-dong', function () {
    return view('products.phottruyendong');
});
Route::get('/phot-thuy-luc', function () {
    return view('products.photthuyluc');
});
Route::get('/phot-xu-ly-chat-long', function () {
    return view('products.xulychatlong');
});
Route::get('/quan-ly-boi-tron', function () {
    return view('products.quanlyboitron');
});
Route::get('/san-pham-boi-tron', function () {
    return view('products.sanphamboitron');
});
Route::get('/san-pham-bao-tri', function () {
    return view('products.spbaotri');
});
Route::get('/giam-sat', function () {
    return view('products.giamsat');
});
Route::get('/truyen-dong', function () {
    return view('products.truyendong');
});
Route::get('/do-kiem-tra', function () {
    return view('products.dovakiemtra');
});
Route::get('/o-bi', function () {
    return view('products.obi');
});
Route::get('/o-bi-con', function () {
    return view('products.obicon');
});
Route::get('/giao-hang', function () {
    return view('homepage.giaohang');
});
Route::get('/bao-mat', function () {
    return view('homepage.baomat');
});
Route::get('/thanh-toan', function () {
    return view('homepage.thanhtoan');
});
Route::get('/phan-biet-hang-gia', function () {
    return view('homepage.phanbietvongbi');
});
Route::get('/uy-quyen', function () {
    return view('homepage.dailyuyquyen');
});
Route::get('/cata-vong-bi', function () {
    return view('homepage.catavongbi');
});
Route::get('/cata-goi-do', function () {
    return view('homepage.catagoidoi');
});
Route::get('/cata-mo', function () {
    return view('homepage.catamo');
});
Route::get('/cata-truyendong', function () {
    return view('homepage.catatruyendong');
});
Route::get('/cata-phot', function () {
    return view('homepage.cataphot');
});
Route::get('/cata-dungcu', function () {
    return view('homepage.catadungcu');
});
Route::get('/cata-olan', function () {
    return view('homepage.cataolan');
});
