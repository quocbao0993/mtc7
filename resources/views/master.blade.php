<!DOCTYPE html>
<html lang="en">
<head>
    @include('share.head')
</head>
<body>
    @include('share.top')
    @yield('content')
    @include('share.foot')
    @include('share.bot')
    @yield('js')
</body>
</html>
