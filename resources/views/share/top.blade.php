<div class="site_wrapper">
    {{-- <div class="topbar dark topbar-padding">
      <div class="container">
        <div class="topbar-left-items">
          <ul class="toplist toppadding pull-left paddtop1">
            <li class="rightl">Chăm sóc khách hàng - HOTLINE:</li>
            <li>0982488345 - 0913665565 - 0397900540</li> --}}

            {{-- <li>EMAIL: XXX</li> --}}
{{--
          </ul>
        </div> --}}
        <!--end left-->

        {{-- <div class="topbar-right-items pull-right">
          <ul class="toplist toppadding"> --}}
            {{-- <li><a href="#"><i class="fa fa-search"></i> &nbsp;</a></li> --}}
            {{-- <li>ĐẠI LÝ ỦY QUYỀN SKF TẠI VIỆT NAM</li>
            <br>
            <li><i class="fa fa-envelope"> Email: email@vongbimientrung.com</i></li>
                                                                                                                                                                                                               </ul>
        </div>
      </div>
    </div> --}}
    {{-- <div class="topbar white">
      <div class="container">
        <div class="topbar-left-items">
          <div class="margin-top1"></div>
          <ul class="toplist toppadding pull-left paddtop1">
            <li class="lineright"><a href="/#">Đăng nhập</a></li>
            <li><a href="/#">Đăng ký</a></li>
          </ul>
         </div> --}}
        <!--end left-->

        {{-- <div class="topbar-middle-logo no-bgcolor"><a href="/#"><img src="/assets/images/logo2.png" width="300" height="105" alt=""/></a></div> --}}

        <!--end middle-->
{{--
        <div class="topbar-right-items pull-right">
          <div class="margin-top1"></div>
          <ul class="toplist toppadding">
            <li class="lineright"><a href="/#">Tìm kiếm</a></li>
            <li class="last"><a href="/contact">Nhận báo giá</a></li> --}}
            {{-- <li class="last"><a href="#">Giỏ hàng</a></li> --}}
          {{-- </ul>
        </div>
      </div>
    </div> --}}
    <div class="clearfix"></div>

    <div id="header">
      <div class="container">
        <div class="navbar red-2 navbar-default yamm ">
          <div class="navbar-header">
            <img class="logo padding:20px" onclick="window.location.href='/#'" src="/assets/images/logo3.png"  width="220" height="100" alt="MTC">
            <button type="button" data-toggle="collapse" data-target="#navbar-collapse-grid" class="navbar-toggle two three"><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
          </div>
          <div id="navbar-collapse-grid" class="navbar-collapse collapse">
            <ul class="nav red-2 navbar-nav">
              <li class="dropdown"> <a href="/#" class="dropdown-toggle active">TRANG CHỦ</a>
              </li>
              <li><a href="/about" class="dropdown-toggle">GIỚI THIỆU</a></li>
              <li class="dropdown yamm-fw"> <a href="/san-pham" class="dropdown-toggle">SẢN PHẨM</a>
                <ul class="dropdown-menu">
                  <li>
                    <!-- Content container to add padding -->
                    <div class="yamm-content">
                      <div class="row">
                        <ul class="col-sm-6 col-md-2 list-unstyled ">
                          <li>
                            <p> <a href="/o-bi"> Ổ BI SKF CÁC LOẠI CHÍNH HÃNG</a></p>
                          </li>
                          <li><a href="/o-bi-con"><img width="58" height="58" alt="Vòng bi SKF chính hãng" src="/assets/products/navbar/obi/obi.png" class="lazyload"><i class="fa fa-angle-right"></i> &nbsp; Ổ BI SKF CHÍNH HÃNG</a></li>
                          <li><a href="/o-lan-con"><img width="58" height="58" alt="Ổ lăn SKF chính hãng" src="/assets/products/navbar/obi/olan.png" class="lazyload"><i class="fa fa-angle-right"></i> &nbsp; Ổ LĂN SKF CHÍNH HÃNG</a></li>
                          <li><a href="/pk-vong-bi"><img width="58" height="58" alt="Phụ kiện vòng bi SKF chính hãng" src="/assets/products/navbar/obi/phukienvongbi.png" class="lazyload"><i class="fa fa-angle-right"></i> &nbsp; PHỤ KIỆN VÒNG BI SKF CHÍNH HÃNG</a></li>
                          <li><a href="/sp-chuyen-biet"><img width="58" height="58" alt="Phụ kiện chuyên biệt SKF chính hãng" src="/assets/products/navbar/obi/pkchuyenbiet.png" class="lazyload"><i class="fa fa-angle-right"></i> &nbsp; SẢN PHẨM CHUYÊN BIỆT</a></li>
                          <li><a href="/o-lan-thanh-ray"><img width="58" height="58" alt="Ổ lăn trên thanh ray SKF chính hãng" src="/assets/products/navbar/obi/olantrenthanhray.png" class="lazyload"><i class="fa fa-angle-right"></i> &nbsp; Ổ LĂN TRÊN THANH RAY</a></li>
                        </ul>
                        <ul class="col-sm-6 col-md-2 list-unstyled ">
                          <li>
                            <p> <a href="/cum-goi-doi-o-lan"> CỤM GỐI ĐỠ VÀ Ổ LĂN </a> </p>
                          </li>
                          <li><a href="/cum-goi-doi-o-lan"><img width="58" height="58" alt="Cụm ổ bi/ vòng bi SKF chính hãng" src="/assets/products/navbar/goidovaolan/cumobi.png" class="lazyload"><i class="fa fa-angle-right"></i> &nbsp; CỤM Ổ BI/ VÒNG BI</a></li>
                          <li><a href="/cum-goi-doi-o-lan"><img width="58" height="58" alt="Cụm ổ lăn SKF chính hãng" src="/assets/products/navbar/goidovaolan/cumolan.png" class="lazyload"><i class="fa fa-angle-right"></i> &nbsp; CỤM Ổ LĂN</a></li>
                          <li><a href="/cum-goi-doi-o-lan"><img width="58" height="58" alt="Gối đỡ ổ lăn SKF chính hãng" src="/assets/products/navbar/goidovaolan/goidoolan.jfif" class="lazyload"><i class="fa fa-angle-right"></i> &nbsp; GỐI ĐỠ Ổ LĂN</a></li>
                          <li><a href="/cum-goi-doi-o-lan"><img width="58" height="58" alt="Cụm hai nửa SKF copper chính hãng" src="/assets/products/navbar/goidovaolan/cooper.png" class="lazyload"><i class="fa fa-angle-right"></i> &nbsp; CỤM HAI NỬA SFK COPPER</a></li>
                          <li><a href="/cum-goi-doi-o-lan"><img width="58" height="58" alt="Cụm ổ lăn SKF ConCentra chính hãng" src="/assets/products/navbar/goidovaolan/concentra.jfif" class="lazyload"><i class="fa fa-angle-right"></i> &nbsp; CỤM Ổ LĂN SFK ConCentra</a></li>
                        </ul>
                        <ul class="col-sm-6 col-md-2 list-unstyled ">
                          <li>
                            <p> <a href="/vong-bi-chinh-xac"> VÒNG BI SIÊU CHÍNH XÁC </a> </p>
                          </li>
                          <li><a href="/vong-bi-chinh-xac"><img width="58" height="58" alt="Ổ bi tiếp xúc góc SKF chính hãng" src="/assets/products/navbar/vongbisieuchinhxac/obitiepxucgoc.jfif" class="lazyload"><i class="fa fa-angle-right"></i> &nbsp; Ổ BI TIẾP XÚC GÓC SKF CHÍNH HÃNG</a></li>
                          <li><a href="/vong-bi-chinh-xac"><img width="58" height="58" alt="Ổ đũa đỡ SKF chính hãng" src="/assets/products/navbar/vongbisieuchinhxac/oduado.png" class="lazyload"><i class="fa fa-angle-right"></i> &nbsp; Ổ ĐŨA ĐỠ SKF CHÍNH HÃNG</a></li>
                          <li><a href="/vong-bi-chinh-xac"><img width="58" height="58" alt="Ổ bi/ vòng bi chặn tiếp xúc 2 chiều SKF chính hãng" src="/assets/products/navbar/vongbisieuchinhxac/obichantiepxucgoc2chieu.jfif" class="lazyload"><i class="fa fa-angle-right"></i> &nbsp; Ổ BI/ VÒNG BI CHẶN TIẾP XÚC GÓC 2 CHIỀU</a></li>
                          <li><a href="/vong-bi-chinh-xac"><img width="58" height="58" alt="Ổ đũa đỡ dọc trục hướng tâm SKF chính hãng" src="/assets/products/navbar/vongbisieuchinhxac/oduadotruchuongtam.jfif" class="lazyload"><i class="fa fa-angle-right"></i> &nbsp; Ổ ĐŨA ĐỠ DỌC TRỤC-HƯỚNG TÂM</a></li>
                          <li><a href="/vong-bi-chinh-xac"><img width="58" height="58" alt="Ổ bi/vòng bi chặn tiếp xúc góc cho bộ truyền vít me SKF chính hãng" src="/assets/products/navbar/vongbisieuchinhxac/obichantiepxucgochuongme.jfif" class="lazyload"><i class="fa fa-angle-right"></i> &nbsp; Ổ BI/ VÒNG BI CHẶN TIẾP XÚC GÓC CHO BỘ TRUYỀN VÍT ME</a></li>
                          <li><a href="/vong-bi-chinh-xac"><img width="58" height="58" alt="Đai ốc khóa có độ chính xác SKF chính hãng" src="/assets/products/navbar/vongbisieuchinhxac/daiockhoa.jfif" class="lazyload"><i class="fa fa-angle-right"></i> &nbsp; ĐAI ỐC KHÓA CÓ ĐỘ CHÍNH XÁC</a></li>
                          <li><a href="/vong-bi-chinh-xac"><img width="58" height="58" alt="Dụng cụ dưỡng đo SKF chính hãng" src="/assets/products/navbar/vongbisieuchinhxac/dungcuduongdo.jfif" class="lazyload"><i class="fa fa-angle-right"></i> &nbsp; DỤNG CỤ DƯỠNG ĐO</a></li>
                        </ul>
                        <ul class="col-sm-6 col-md-2 list-unstyled ">
                          <li>
                            <p> <a href="/o-vanh-xoay"> Ổ VÀNH XOAY </a> </p>
                          </li>
                          <li><a href="/o-vanh-xoay"><img width="58" height="58" alt="" src="/assets/products/navbar/ovanhxoay/4diem.png" class="lazyload"><i class="fa fa-angle-right"></i> &nbsp; Ổ VÀNH XOAY VỚI Ổ LĂN 4 ĐIỂM TIẾP XÚC</a></li>
                          <li><a href="/o-vanh-xoay"><img width="58" height="58" alt="" src="/assets/products/navbar/ovanhxoay/8diem.png" class="lazyload"><i class="fa fa-angle-right"></i> &nbsp; Ổ VÀNH XOAY VỚI Ổ LĂN 8 ĐIỂM TIẾP XÚC</a></li>
                          <li><a href="/o-vanh-xoay"><img width="58" height="58" alt="" src="/assets/products/navbar/ovanhxoay/oduacheo.png" class="lazyload"><i class="fa fa-angle-right"></i> &nbsp; Ổ VÀNH XOAY CÓ Ổ ĐŨA CHÉO</a></li>
                          <li><a href="/o-vanh-xoay"><img width="58" height="58" alt="" src="/assets/products/navbar/ovanhxoay/3hang.png" class="lazyload"><i class="fa fa-angle-right"></i> &nbsp; Ổ VÀNH XOAY CÓ Ổ BA HÀNG</a></li>
                          <li><a href="/o-vanh-xoay"><img width="58" height="58" alt="" src="/assets/products/navbar/ovanhxoay/coduongluonday.png" class="lazyload"><i class="fa fa-angle-right"></i> &nbsp; Ổ VÀNH XOAY CÓ VÒNG BI CÓ ĐƯỜNG LUỒN DÂY</a></li>
                          <li><a href="/o-vanh-xoay"><img width="58" height="58" alt="" src="/assets/products/navbar/ovanhxoay/xoaytuychinh.png" class="lazyload"><i class="fa fa-angle-right"></i> &nbsp; Ổ VÀNH XOAY TÙY CHỈNH</a></li>
                        </ul>
                        <ul class="col-sm-6 col-md-2 list-unstyled ">
                            <li>
                              <p> <a href="/o-truot"> Ổ TRƯỢT </a> </p>
                            </li>
                            <li><a href="/o-truot"><img width="58" height="58" alt="" src="/assets/products/navbar/otruot/otruottangtrong.png" class="lazyload"><i class="fa fa-angle-right"></i> &nbsp; Ổ TRƯỢT TANG TRỐNG VÀ KHỚP CẦU CÓ THANH REN</a></li>
                            <li><a href="/o-truot"><img width="58" height="58" alt="" src="/assets/products/navbar/otruot/onglot.png" class="lazyload"><i class="fa fa-angle-right"></i> &nbsp; ỐNG LÓT, VÒNG ĐỆM CHẶN VÀ TẤM ĐỆM</a></li>
                        </ul>
                        <ul class="col-sm-6 col-md-2 list-unstyled ">
                            <li>
                              <p> <a href="/san-pham"> CÁC LOẠI SẢN PHẨM KHÁC </p>
                            </li>
                            <li><a href="/#"><img width="58" height="58" alt="" src="/assets/products/navbar/cacloaisanphamkhac/vongbitutinh.png" class="lazyload"><i class="fa fa-angle-right"></i> &nbsp; CÁC HỆ THỐNG VÀ VÒNG BI TỪ TÍNH</a></li>
                            <li><a href="/phot-chan-dau"><img width="58" height="58" alt="" src="/assets/products/navbar/cacloaisanphamkhac/photdaucongnghiep.png" class="lazyload"><i class="fa fa-angle-right"></i> &nbsp; PHỚT CHẶN DẦU CÔNG NGHIỆP</a></li>
                            <li><a href="/quan-ly-boi-tron"><img width="58" height="58" alt="" src="/assets/products/navbar/cacloaisanphamkhac/quanlyboitron.png" class="lazyload"><i class="fa fa-angle-right"></i> &nbsp; QUẢN LÝ BÔI TRƠN</a></li>
                            <li><a href="/san-pham-bao-tri"><img width="58" height="58" alt="" src="/assets/products/navbar/cacloaisanphamkhac/spbaotri.png" class="lazyload"><i class="fa fa-angle-right"></i> &nbsp; SẢN PHẨM BẢO TRÌ</a></li>
                            <li><a href="/giam-sat"><img width="58" height="58" alt="" src="/assets/products/navbar/cacloaisanphamkhac/giamsat.png" class="lazyload"><i class="fa fa-angle-right"></i> &nbsp; HỆ THỐNG GIÁM SÁT TÌNH TRẠNG HOẠT ĐỘNG</a></li>
                            <li><a href="/truyen-dong"><img width="58" height="58" alt="" src="/assets/products/navbar/cacloaisanphamkhac/truyendong.png" class="lazyload"><i class="fa fa-angle-right"></i> &nbsp; CÁC GIẢI PHÁP TRUYỀN ĐỘNG</a></li>
                            <li><a href="/do-kiem-tra"><img width="58" height="58" alt="" src="/assets/products/navbar/cacloaisanphamkhac/dovakiemtra.png" class="lazyload"><i class="fa fa-angle-right"></i> &nbsp; THIẾT BỊ ĐO VÀ KIỂM TRA</a></li>
                        </ul>
                      </div>
                    </div>
                  </li>
                </ul>
              </li>

              <li><a href="/catalogue" class="dropdown-toggle">CATALOGUE</a></li>

              <li><a href="/contact" class="dropdown-toggle">LIÊN HỆ</a></li>

              <li class="dropdown yamm-fw"> <a href="/uy-quyen" class="dropdown-toggle">Đại lý ủy quyền SKF</a>
                {{-- <ul class="dropdown-menu">
                  <li>
                    <!-- Content container to add padding -->
                    <div class="yamm-content">
                      <div class="row">
                        <ul class="col-sm-6 col-md-3 list-unstyled ">
                          <li>
                            <li><a href="/tin-tuc"><img width="58" height="58" alt="" src="https://www.skf.com/binaries/pub12/Images/0901d19680bdb6fc-SKFauthenticatergbroundedoutline1024px_tcm_12-568794.png" class="lazyload"><i class="fa fa-angle-right"></i> &nbsp; SKF Authenticate</a></li>
                          </li> --}}
                          {{-- <li><a href="shop-men.html"><i class="fa fa-angle-right"></i> &nbsp; Premium Wear</a></li> --}}
                        {{-- </ul>
                        <ul class="col-sm-6 col-md-3 list-unstyled ">
                          <li> --}}
                            {{-- <a href="/tu-van"> Tư vấn - Review </a> --}}
                          {{-- </li> --}}
                          {{-- <li><a href="shop-men.html"><i class="fa fa-angle-right"></i> &nbsp; Formal Shoes</a></li>
                          <li><a href="shop-men.html"><i class="fa fa-angle-right"></i> &nbsp; Sandals & Floaters</a></li>
                          <li><a href="shop-men.html"><i class="fa fa-angle-right"></i> &nbsp; Boots</a></li>
                          <li><a href="shop-men.html"><i class="fa fa-angle-right"></i> &nbsp; Casual Shoes</a></li>
                          <li><a href="shop-men.html"><i class="fa fa-angle-right"></i> &nbsp; Sports Shoes</a></li> --}}
                        {{-- </ul> --}}
                        {{-- <ul class="col-sm-6 col-md-3 list-unstyled ">
                          <li>
                            <p> Watches </p>
                          </li>
                          <li><a href="shop-men.html"><i class="fa fa-angle-right"></i> &nbsp; Fasttrack</a></li>
                          <li><a href="shop-men.html"><i class="fa fa-angle-right"></i> &nbsp; Casio</a></li>
                          <li><a href="shop-men.html"><i class="fa fa-angle-right"></i> &nbsp; Timex</a></li>
                          <li><a href="shop-men.html"><i class="fa fa-angle-right"></i> &nbsp; Nixon</a></li>
                          <li><a href="shop-men.html"><i class="fa fa-angle-right"></i> &nbsp; Nine West</a></li>
                        </ul>
                        <ul class="col-sm-6 col-md-3 list-unstyled ">
                          <li>
                            <p> Accessories </p>
                          </li>
                          <li><a href="shop-men.html"><i class="fa fa-angle-right"></i> &nbsp; Bags</a></li>
                          <li><a href="shop-men.html"><i class="fa fa-angle-right"></i> &nbsp; Belts</a></li>
                          <li><a href="shop-men.html"><i class="fa fa-angle-right"></i> &nbsp; Sunglasses</a></li>
                          <li><a href="shop-men.html"><i class="fa fa-angle-right"></i> &nbsp; Shaving Kits & Needs</a></li>
                          <li><a href="shop-men.html"><i class="fa fa-angle-right"></i> &nbsp; Bath Needs</a></li>
                        </ul> --}}
                      </div>
                    </div>
                  </li>
                </ul>
              </li>
            </ul>
            <br/>
            {{-- <a href="/contact" class="dropdown-toggle pull-right btn btn-red btn-xround">HOTLINE</a> </div> --}}
        </div>
      </div>
    </div>
