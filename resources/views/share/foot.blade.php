<section class="section-light sec-padding">
    <div class="container">
      <div class="row">
        <div class="col-md-3 clearfix">
          <div class="footer-logo"><img width="200" height="58" src="/assets/images/flogo.png" alt=""></div>
          <ul class="address-info-2">
            <li>Address: 04 Trần Kế Xương, Quận Hải Châu, Thành phố Đà Nẵng</li>
            <a href="tel:+840982488345"><li><i class="fa fa-phone"></i> Phone 1: 0982488345 </li></a>
            <a href="tel:+840913665565"><li><i class="fa fa-phone"></i> Phone 2: 0913665565 </li></a>
            <a href="tel:+840889041468"><li><i class="fa fa-phone"></i> Phone 3: 0889041468 </li></a>
            <a href="mailto: mtc.skfdanang@gmail.com"><li><i class="fa fa-envelope"></i> Email: mtc.skfdanang@gmail.com </li></a>
          </ul>
        </div>
        <!--end item-->


        <div class="col-md-3 clearfix">
            <h4 class="uppercase footer-title two less-mar3 roboto-slab">TÀI LIỆU SKF</h4>
            <div class="clearfix"></div>
            <div class="footer-title-bottomstrip"></div>
            <ul class="usefull-links-2">
              <li><a href="/tin-tuc"><i class="fa fa-angle-right"></i> SKF AUTHENTICATE</a></li>
              <li><a href="/catalogue"><i class="fa fa-angle-right"></i> Catalogue công ty</a></li>
              <li><a href="/phan-biet-hang-gia"><i class="fa fa-angle-right"></i> Phân biệt vòng bi giả</a></li>
              {{-- <li><a href="#"><i class="fa fa-angle-right"></i> Tư vấn lựa chọn vòng bi</a></li>
              <li><a href="/giao-hang"><i class="fa fa-angle-right"></i> Giao hàng và đổi trả hàng</a></li> --}}
            </ul>
          </div>
        <!--end item-->

        <div class="col-md-3 clearfix">
          <h4 class="uppercase footer-title two less-mar3 roboto-slab">VỀ CHÚNG TÔI</h4>
          <div class="clearfix"></div>
          <div class="footer-title-bottomstrip"></div>
          <ul class="usefull-links-2">
            <li><a href="/uy-quyen"><i class="fa fa-angle-right"></i> Đại lý ủy quyền SKF</a></li>
            <li><a href="/about"><i class="fa fa-angle-right"></i> Giới thiệu chung</a></li>
            <li><a href="https://zalo.me/0889041468" target="_blank"> <i class="fa fa-angle-right"></i> Dịch vụ kỹ thuật</a></li>
            <li><a href="https://zalo.me/0889041468" target="_blank"><i class="fa fa-angle-right"></i> Tư vấn lựa chọn vòng bi</a></li>
            <li><a href="/giao-hang"><i class="fa fa-angle-right"></i> Giao hàng và đổi trả hàng</a></li>
          </ul>
        </div>
        <!--end item-->
        <div class="col-md-3 clearfix">
            <h4 class="uppercase footer-title two less-mar3 roboto-slab">THÔNG TIN CHUNG</h4>
            <div class="clearfix"></div>
            <div class="footer-title-bottomstrip"></div>
            <ul class="usefull-links-2">
              <li><a href="/bao-mat"><i class="fa fa-angle-right"></i> Chính sách bảo mật</a></li>
              <li><a href="/giao-hang"><i class="fa fa-angle-right"></i> Bảo hành</a></li>
              <li><a href="/bao-mat"><i class="fa fa-angle-right"></i> Câu hỏi thường gặp</a></li>
              <li><a href="/thanh-toan"><i class="fa fa-angle-right"></i> Phương thức thanh toán</a></li>
              <li><a href="/contact"><i class="fa fa-angle-right"></i> Liên hệ</a></li>
            </ul>
          </div>
        {{-- <div class="col-md-3 clearfix">
          <div class="item-holder">
            <h4 class="uppercase footer-title two less-mar3 roboto-slab">Featured</h4>
            <div class="footer-title-bottomstrip"></div>
            <div class="clearfix"></div>
            <div class="image-left"><img src="http://placehold.it/80x80" alt=""></div>
            <div class="text-box-right">
              <h6 class="less-mar3 nopadding roboto-slab nopadding"><a href="#">Clean And Modern</a></h6>
              <p>$12.59</p>
              <div class="footer-post-info"><span><i class="fa fa-star"></i></span><span><i class="fa fa-star"></i></span><span><i class="fa fa-star"></i></span><span><i class="fa fa-star"></i></span><span><i class="fa fa-star"></i></span></div>
            </div>
            <div class="divider-line solid light margin"></div>
            <div class="clearfix"></div>
            <div class="image-left"><img src="http://placehold.it/80x80" alt=""></div>
            <div class="text-box-right">
              <h6 class="less-mar3 roboto-slab nopadding"><a href="#">Layered PSD Files</a></h6>
              <p>Lorem ipsum dolor sit</p>
              <div class="footer-post-info"><span><i class="fa fa-star"></i></span><span><i class="fa fa-star"></i></span><span><i class="fa fa-star"></i></span><span><i class="fa fa-star"></i></span><span><i class="fa fa-star"></i></span></div>
            </div>
          </div>
        </div> --}}
        <!--end item-->

      </div>
    </div>
  </section>
  <!--end section-->
  <div class="clearfix"></div>
  <a href="#" class="scrollup red-4"></a><!-- end scroll to top of the page-->
  <a href="https://zalo.me/0889041468" id="linkzalo" target="_blank" rel="noopener noreferrer">
    <div id="fcta-zalo-tracking" class="fcta-zalo-mess">
        <span id="fcta-zalo-tracking">Chat hỗ trợ</span>
    </div>
    <div class="fcta-zalo-vi-tri-nut">
        <div id="fcta-zalo-tracking" class="fcta-zalo-nen-nut">
            <div id="fcta-zalo-tracking" class="fcta-zalo-ben-trong-nut"> <svg
                    xmlns="http://www.w3.org/2000/svg" viewBox="0 0 460.1 436.6">
                    <path fill="currentColor" class="st0"
                        d="M82.6 380.9c-1.8-.8-3.1-1.7-1-3.5 1.3-1 2.7-1.9 4.1-2.8 13.1-8.5 25.4-17.8 33.5-31.5 6.8-11.4 5.7-18.1-2.8-26.5C69 269.2 48.2 212.5 58.6 145.5 64.5 107.7 81.8 75 107 46.6c15.2-17.2 33.3-31.1 53.1-42.7 1.2-.7 2.9-.9 3.1-2.7-.4-1-1.1-.7-1.7-.7-33.7 0-67.4-.7-101 .2C28.3 1.7.5 26.6.6 62.3c.2 104.3 0 208.6 0 313 0 32.4 24.7 59.5 57 60.7 27.3 1.1 54.6.2 82 .1 2 .1 4 .2 6 .2H290c36 0 72 .2 108 0 33.4 0 60.5-27 60.5-60.3v-.6-58.5c0-1.4.5-2.9-.4-4.4-1.8.1-2.5 1.6-3.5 2.6-19.4 19.5-42.3 35.2-67.4 46.3-61.5 27.1-124.1 29-187.6 7.2-5.5-2-11.5-2.2-17.2-.8-8.4 2.1-16.7 4.6-25 7.1-24.4 7.6-49.3 11-74.8 6zm72.5-168.5c1.7-2.2 2.6-3.5 3.6-4.8 13.1-16.6 26.2-33.2 39.3-49.9 3.8-4.8 7.6-9.7 10-15.5 2.8-6.6-.2-12.8-7-15.2-3-.9-6.2-1.3-9.4-1.1-17.8-.1-35.7-.1-53.5 0-2.5 0-5 .3-7.4.9-5.6 1.4-9 7.1-7.6 12.8 1 3.8 4 6.8 7.8 7.7 2.4.6 4.9.9 7.4.8 10.8.1 21.7 0 32.5.1 1.2 0 2.7-.8 3.6 1-.9 1.2-1.8 2.4-2.7 3.5-15.5 19.6-30.9 39.3-46.4 58.9-3.8 4.9-5.8 10.3-3 16.3s8.5 7.1 14.3 7.5c4.6.3 9.3.1 14 .1 16.2 0 32.3.1 48.5-.1 8.6-.1 13.2-5.3 12.3-13.3-.7-6.3-5-9.6-13-9.7-14.1-.1-28.2 0-43.3 0zm116-52.6c-12.5-10.9-26.3-11.6-39.8-3.6-16.4 9.6-22.4 25.3-20.4 43.5 1.9 17 9.3 30.9 27.1 36.6 11.1 3.6 21.4 2.3 30.5-5.1 2.4-1.9 3.1-1.5 4.8.6 3.3 4.2 9 5.8 14 3.9 5-1.5 8.3-6.1 8.3-11.3.1-20 .2-40 0-60-.1-8-7.6-13.1-15.4-11.5-4.3.9-6.7 3.8-9.1 6.9zm69.3 37.1c-.4 25 20.3 43.9 46.3 41.3 23.9-2.4 39.4-20.3 38.6-45.6-.8-25-19.4-42.1-44.9-41.3-23.9.7-40.8 19.9-40 45.6zm-8.8-19.9c0-15.7.1-31.3 0-47 0-8-5.1-13-12.7-12.9-7.4.1-12.3 5.1-12.4 12.8-.1 4.7 0 9.3 0 14v79.5c0 6.2 3.8 11.6 8.8 12.9 6.9 1.9 14-2.2 15.8-9.1.3-1.2.5-2.4.4-3.7.2-15.5.1-31 .1-46.5z">
                    </path>
                </svg></div>
            <div id="fcta-zalo-tracking" class="fcta-zalo-text">Chat ngay</div>
        </div>
    </div>
</a>

<style>
    @keyframes zoom {
        0% {
            transform: scale(.5);
            opacity: 0
        }

        50% {
            opacity: 1
        }

        to {
            opacity: 0;
            transform: scale(1)
        }
    }

    @keyframes lucidgenzalo {
        0% to {
            transform: rotate(-25deg)
        }

        50% {
            transform: rotate(25deg)
        }
    }

    .jscroll-to-top {
        bottom: 100px
    }

    .fcta-zalo-ben-trong-nut svg path {
        fill: #fff
    }

    .fcta-zalo-vi-tri-nut {
        position: fixed;
        bottom: 24px;
        right: 20px;
        z-index: 999
    }

    .fcta-zalo-nen-nut,
    div.fcta-zalo-mess {
        box-shadow: 0 1px 6px rgba(0, 0, 0, .06), 0 2px 32px rgba(0, 0, 0, .16)
    }

    .fcta-zalo-nen-nut {
        width: 50px;
        height: 50px;
        text-align: center;
        color: #fff;
        background: #0068ff;
        border-radius: 50%;
        position: relative
    }

    .fcta-zalo-nen-nut::after,
    .fcta-zalo-nen-nut::before {
        content: "";
        position: absolute;
        border: 1px solid #0068ff;
        background: #0068ff80;
        z-index: -1;
        left: -20px;
        right: -20px;
        top: -20px;
        bottom: -20px;
        border-radius: 50%;
        animation: zoom 1.9s linear infinite
    }

    .fcta-zalo-nen-nut::after {
        animation-delay: .4s
    }

    .fcta-zalo-ben-trong-nut,
    .fcta-zalo-ben-trong-nut i {
        transition: all 1s
    }

    .fcta-zalo-ben-trong-nut {
        position: absolute;
        text-align: center;
        width: 60%;
        height: 60%;
        left: 10px;
        bottom: 25px;
        line-height: 70px;
        font-size: 25px;
        opacity: 1
    }

    .fcta-zalo-ben-trong-nut i {
        animation: lucidgenzalo 1s linear infinite
    }

    .fcta-zalo-nen-nut:hover .fcta-zalo-ben-trong-nut,
    .fcta-zalo-text {
        opacity: 0
    }

    .fcta-zalo-nen-nut:hover i {
        transform: scale(.5);
        transition: all .5s ease-in
    }

    .fcta-zalo-text a {
        text-decoration: none;
        color: #fff
    }

    .fcta-zalo-text {
        position: absolute;
        top: 6px;
        text-transform: uppercase;
        font-size: 12px;
        font-weight: 700;
        transform: scaleX(-1);
        transition: all .5s;
        line-height: 1.5
    }

    .fcta-zalo-nen-nut:hover .fcta-zalo-text {
        transform: scaleX(1);
        opacity: 1
    }

    div.fcta-zalo-mess {
        position: fixed;
        bottom: 29px;
        right: 58px;
        z-index: 99;
        background: #fff;
        padding: 7px 25px 7px 15px;
        color: #0068ff;
        border-radius: 50px 0 0 50px;
        font-weight: 700;
        font-size: 15px
    }

    .fcta-zalo-mess span {
        color: #0068ff !important
    }

    span#fcta-zalo-tracking {
        font-family: Roboto;
        line-height: 1.5
    }

    .fcta-zalo-text {
        font-family: Roboto
    }
</style>

<script>
    if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
        document.getElementById("linkzalo").href = "https://zalo.me/0889041468";
    }
</script>
<script>
    $(".carousel").swipe({

      swipe: function(event, direction, distance, duration, fingerCount, fingerData) {

        if (direction == 'left') $(this).carousel('next');
        if (direction == 'right') $(this).carousel('prev');

      },
      allowPageScroll:"vertical"

    });
</script>
