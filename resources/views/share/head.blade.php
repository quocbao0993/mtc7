<title>MTC - ĐẠI LÝ SKF CHÍNH HÃNG TẠI MIỀN TRUNG</title>
<meta charset="utf-8">
<!-- Meta -->
<meta name="keywords" content="" />
<meta name="author" content="">
<meta name="robots" content="" />
<meta name="description" content="" />

<!-- this styles only adds some repairs on idevices  -->
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<!-- Favicon -->
<link rel="shortcut icon" href="images/favicon.ico">

<!-- Google fonts - witch you want to use - (rest you can just remove) -->
<link href='https://fonts.googleapis.com/css?family=Open+Sans:300,300italic,400,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Roboto:100,200,300,400,500,600,700,800,900' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700' rel='stylesheet' type='text/css'>

<!--[if lt IE 9]>
<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->

<!-- Stylesheets -->
<link rel="stylesheet" media="screen" href="/assets/js/bootstrap/bootstrap.min.css" type="text/css" />
<link rel="stylesheet" href="/assets/js/mainmenu/menu.css" type="text/css" />
<link rel="stylesheet" href="/assets/css/default.css" type="text/css" />
<link rel="stylesheet" href="/assets/css/layouts.css" type="text/css" />
<link rel="stylesheet" href="/assets/css/shortcodes.css" type="text/css" />
<link rel="stylesheet" href="/assets/css/font-awesome/css/font-awesome.min.css">
<link rel="stylesheet" media="screen" href="/assets/css/responsive-leyouts.css" type="text/css" />
<link rel="stylesheet" href="/assets/js/masterslider/style/masterslider.css" />
<link rel="stylesheet" type="text/css" href="/assets/css/Simple-Line-Icons-Webfont/simple-line-icons.css" media="screen" />
<link rel="stylesheet" href="/assets/css/et-line-font/et-line-font.css">
<link rel="stylesheet" href="css/colors/lightblue.css" />


<!-- Remove the below comments to use your color option -->
<!--<link rel="stylesheet" href="css/colors/lightblue.css" />-->
<!--<link rel="stylesheet" href="css/colors/orange.css" />-->
<!--<link rel="stylesheet" href="css/colors/green.css" />-->
<!--<link rel="stylesheet" href="css/colors/pink.css" />-->
<!--<link rel="stylesheet" href="css/colors/red.css" />-->
<!--<link rel="stylesheet" href="css/colors/purple.css" />-->
<!--<link rel="stylesheet" href="css/colors/bridge.css" />-->
<!--<link rel="stylesheet" href="css/colors/yellow.css" />-->
<!--<link rel="stylesheet" href="css/colors/violet.css" />-->
<!--<link rel="stylesheet" href="css/colors/cyan.css" />-->
<!--<link rel="stylesheet" href="css/colors/mossgreen.css" />-->
