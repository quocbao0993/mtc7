@extends('master')
@section('content')
    <div class="container">

        <h1 style="font-size:20px ;font-weight:700">Catalogue vòng bi SKF</h1>


        <br>
        <style>
            .download-list .item {
                float: left;
                width: 50%;
                padding: 15px;
                padding-right: 30px;
            }

            .download-list .item.col2 {
                padding-left: 30px;
                padding-right: 15px;
            }

            .download-list .item .inner {
                border-bottom: 1px dotted #ccc;
                position: relative;
                padding-right: 50px;
                min-height: 56px;
            }

            .download-list .item .inner .tt {
                padding: 0 0 5px;
                margin: 0;
                font-size: 16px;
                display: block;
                white-space: nowrap;
                overflow: hidden;
                text-overflow: ellipsis;
                font-weight: 400;
            }

            .download-list .item .inner .btn {
                position: absolute;
                right: 0;
            }

            .download-list .item .inner p {
                display: block;
                white-space: nowrap;
                overflow: hidden;
                text-overflow: ellipsis;
            }

            @media (max-width: 991px) {
                .download-list .item {
                    width: auto;
                    float: none;
                    padding-right: 15px;
                }

                .download-list .item.col2 {
                    padding-left: 15px;
                }
            }
        </style>

        <div style="margin-top: -50px">
            <div class="page-header"></div>
            <div class="download-list row">

                <div class="item">
                    <div class="inner">
                        <a title="Tải xuống Tài liệu vòng bi tang trống hai dãy tự lựa" target="_blank"
                            href="/assets/catalogue/vongbi/vong-bi-tang-trong-hai-day.pdf"
                            class="btn btn-info"><i class="fa fa-download"></i></a>

                        <h2 class="tt"><a title="Tải xuống Tài liệu vòng bi tang trống hai dãy tự lựa" target="_blank"
                                href="/assets/catalogue/vongbi/vong-bi-tang-trong-hai-day.pdf">Tài liệu vòng
                                bi tang trống hai dãy tự lựa</a></h2>
                        <p></p>
                    </div>
                </div>
                <div class="item">
                    <div class="inner">
                        <a title="Tải xuống Tài liệu vòng bi 2 nửa SKF" target="_blank"
                            href="/assets/catalogue/vongbi/SKF-vong-bi-2-nua-SKF.pdf" class="btn btn-info"><i
                                class="fa fa-download"></i></a>

                        <h2 class="tt"><a title="Tải xuống Tài liệu vòng bi 2 nửa SKF" target="_blank"
                                href="/assets/catalogue/vongbi/SKF-vong-bi-2-nua-SKF.pdf">Tài liệu vòng bi 2
                                nửa SKF</a></h2>
                        <p></p>
                    </div>
                </div>
                <div class="item">
                    <div class="inner">
                        <a title="Tải xuống Tính toán tuổi thọ vòng bi" target="_blank"
                            href="/assets/catalogue/vongbi/SKF-tinh-toan-tuoi-tho-vong-bi.pdf"
                            class="btn btn-info"><i class="fa fa-download"></i></a>

                        <h2 class="tt"><a title="Tải xuống Tính toán tuổi thọ vòng bi" target="_blank"
                                href="/assets/catalogue/vongbi/SKF-tinh-toan-tuoi-tho-vong-bi.pdf">Tính toán
                                tuổi thọ vòng bi</a></h2>
                        <p></p>
                    </div>
                </div>
                <div class="item">
                    <div class="inner">
                        <a title="Tải xuống Quy định khe hở vòng bi tang trống SKF" target="_blank"
                            href="/assets/catalogue/vongbi/SKF-khe-ho-vong-bi-tang-trong.pdf"
                            class="btn btn-info"><i class="fa fa-download"></i></a>

                        <h2 class="tt"><a title="Tải xuống Quy định khe hở vòng bi tang trống SKF" target="_blank"
                                href="/assets/catalogue/vongbi/SKF-khe-ho-vong-bi-tang-trong.pdf">Quy định khe
                                hở vòng bi tang trống SKF</a></h2>
                        <p></p>
                    </div>
                </div>
                <div class="item">
                    <div class="inner">
                        <a title="Tải xuống Quy định khe hở vòng bi cầu SKF" target="_blank"
                            href="/assets/catalogue/vongbi/SKF-khe-ho-vong-bi-cau.pdf" class="btn btn-info"><i
                                class="fa fa-download"></i></a>

                        <h2 class="tt"><a title="Tải xuống Quy định khe hở vòng bi cầu SKF" target="_blank"
                                href="/assets/catalogue/vongbi/SKF-khe-ho-vong-bi-cau.pdf">Quy định khe hở
                                vòng bi cầu SKF</a></h2>
                        <p></p>
                    </div>
                </div>
                <div class="item">
                    <div class="inner">
                        <a title="Tải xuống Quy định ký hiệu vòng bi SKF" target="_blank"
                            href="/assets/catalogue/vongbi/Quy_dinh_KyhieuvongbiSKF.pdf" class="btn btn-info"><i
                                class="fa fa-download"></i></a>

                        <h2 class="tt"><a title="Tải xuống Quy định ký hiệu vòng bi SKF" target="_blank"
                                href="/assets/catalogue/vongbi/Quy_dinh_KyhieuvongbiSKF.pdf">Quy định ký hiệu vòng bi SKF</a>
                        </h2>
                        <p></p>
                    </div>
                </div>
                <div class="item">
                    <div class="inner">
                        <a title="Tải xuống Ổ bi đỡ 1 dãy có rãnh tra bi" target="_blank"
                            href="/assets/catalogue/vongbi/1-do1day_ranhtrabi.pdf" class="btn btn-info"><i
                                class="fa fa-download"></i></a>

                        <h2 class="tt"><a title="Tải xuống Ổ bi đỡ 1 dãy có rãnh tra bi" target="_blank"
                                href="/assets/catalogue/vongbi/1-do1day_ranhtrabi.pdf">Ổ bi đỡ 1 dãy có rãnh tra bi</a></h2>
                        <p></p>
                    </div>
                </div>
                <div class="item">
                    <div class="inner">
                        <a title="Tải xuống Ổ bi đỡ 1 dãy" target="_blank" href="/assets/catalogue/vongbi/O_bi_do_motday.pdf"
                            class="btn btn-info"><i class="fa fa-download"></i></a>

                        <h2 class="tt"><a title="Tải xuống Ổ bi đỡ 1 dãy" target="_blank"
                                href="/assets/catalogue/vongbi/O_bi_do_motday.pdf">Ổ bi đỡ 1 dãy</a></h2>
                        <p></p>
                    </div>
                </div>
                <div class="item">
                    <div class="inner">
                        <a title="Tải xuống Tài liệu vòng bi chịu nhiệt SKF" target="_blank"
                            href="/assets/catalogue/vongbi/tai _lieu_vong_bi_chiu_nhiet_SKF.pdf" class="btn btn-info"><i
                                class="fa fa-download"></i></a>

                        <h2 class="tt"><a title="Tải xuống Tài liệu vòng bi chịu nhiệt SKF" target="_blank"
                                href="/assets/catalogue/vongbi/tai _lieu_vong_bi_chiu_nhiet_SKF.pdf">Tài liệu vòng bi chịu nhiệt
                                SKF</a></h2>
                        <p></p>
                    </div>
                </div>
                <div class="item">
                    <div class="inner">
                        <a title="Tải xuống Pulley SKF" target="_blank" href="/assets/catalogue/vongbi/pulley_SKF.pdf"
                            class="btn btn-info"><i class="fa fa-download"></i></a>

                        <h2 class="tt"><a title="Tải xuống Pulley SKF" target="_blank"
                                href="/assets/catalogue/vongbi/pulley_SKF.pdf">Pulley SKF</a></h2>
                        <p></p>
                    </div>
                </div>
                <div class="item">
                    <div class="inner">
                        <a title="Tải xuống Ống lót côn và ống trung gian" target="_blank"
                            href="/assets/catalogue/vongbi/ong_lot.pdf" class="btn btn-info"><i
                                class="fa fa-download"></i></a>

                        <h2 class="tt"><a title="Tải xuống Ống lót côn và ống trung gian" target="_blank"
                                href="/assets/catalogue/vongbi/ong_lot.pdf">Ống lót côn và ống trung gian</a></h2>
                        <p></p>
                    </div>
                </div>
                <div class="item">
                    <div class="inner">
                        <a title="Tải xuống Tài liệu vòng bi côn SKF" target="_blank"
                            href="/assets/catalogue/vongbi/vong-bi-con-skf.pdf" class="btn btn-info"><i
                                class="fa fa-download"></i></a>

                        <h2 class="tt"><a title="Tải xuống Tài liệu vòng bi côn SKF" target="_blank"
                                href="/assets/catalogue/vongbi/vong-bi-con-skf.pdf">Tài liệu vòng bi côn SKF</a></h2>
                        <p></p>
                    </div>
                </div>
                <div class="item">
                    <div class="inner">
                        <a title="Tải xuống Tài liệu vòng bi mặt cầu GE" target="_blank"
                            href="/assets/catalogue/vongbi/vong-bi-mat-cau.pdf" class="btn btn-info"><i
                                class="fa fa-download"></i></a>

                        <h2 class="tt"><a title="Tải xuống Tài liệu vòng bi mặt cầu GE" target="_blank"
                                href="/assets/catalogue/vongbi/vong-bi-mat-cau.pdf">Tài liệu vòng bi mặt cầu GE</a></h2>
                        <p></p>
                    </div>
                </div>
                <div class="item">
                    <div class="inner">
                        <a title="Tải xuống Tài liệu kỹ thuật tổng hợp về Vòng bi SKF" target="_blank"
                            href="/assets/catalogue/vongbi/SKF.pdf" class="btn btn-info"><i class="fa fa-download"></i></a>

                        <h2 class="tt"><a title="Tải xuống Tài liệu kỹ thuật tổng hợp về Vòng bi SKF" target="_blank"
                                href="/assets/catalogue/vongbi/SKF.pdf">Tài liệu kỹ thuật tổng hợp về Vòng bi SKF</a></h2>
                        <p></p>
                    </div>
                </div>
                <div class="item">
                    <div class="inner">
                        <a title="Tải xuống Tài liệu tra cứu vòng bi SKF giả" target="_blank"
                            href="/assets/catalogue/vongbi/SKF-Authenticate.pdf" class="btn btn-info"><i
                                class="fa fa-download"></i></a>

                        <h2 class="tt"><a title="Tải xuống Tài liệu tra cứu vòng bi SKF giả" target="_blank"
                                href="/assets/catalogue/vongbi/SKF-Authenticate.pdf">Tài liệu tra cứu vòng bi SKF giả</a></h2>
                        <p></p>
                    </div>
                </div>

                <div class="clearfix"></div>

            </div>
        </div>


    </div>
@endsection
