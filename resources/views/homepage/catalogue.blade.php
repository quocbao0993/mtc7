@extends('master')
@section('content')

<section class="sec-padding">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 ">
                <h4 class="section-title-7"><span class="roboto-slab uppercase">Catalogue sản phẩm SKF chính hãng</span></h4>
            </div>
            <!--end title-->

            <div class="col-md-3 col-sm-6 bmargin">
                <div class="shop-product-holder">
                    <a href="/cata-vong-bi">
                        <div class="image-holder">

                            <img src="/assets/products/navbar/obi/obi.png"
                                alt="" class="center" width="150" height="150">
                        </div>
                    </a>
                </div>
                <div class="clearfix"></div>
                <br>
                <h5 class="less-mar1 roboto-slab"><a href="/cata-vong-bi">Catalogue vòng bi SKF</a>
                </h5>
                <p></p>
                <h5 class="text-red-4"></h5>
                <br>
                {{-- <a class="btn btn-red-4 btn-small" href="#"></a> --}}
            </div>
            <!--end item-->

            <div class="col-md-3 col-sm-6 bmargin">
                <div class="shop-product-holder">
                    <a href="/cata-goi-do">
                        <div class="image-holder">

                            <img src="/assets/products/navbar/goidovaolan/goidoolan.jfif"
                                alt="" class="center" width="150" height="150">
                        </div>
                    </a>
                </div>
                <div class="clearfix"></div>
                <br>
                <h5 class="less-mar1 roboto-slab"><a href="/cata-goi-do">Catalogue gối đỡ SKF</a>
                </h5>
                <p> </p>
                <h5 class="text-red-4"></h5>
                <br>
                {{-- <a class="btn btn-red-4 btn-small" href="#"></a> --}}
            </div>
            <!--end item-->

            <div class="col-md-3 col-sm-6 bmargin">
                <div class="shop-product-holder">
                    <a href="/cata-mo">
                        <div class="image-holder">

                            <img src="/assets/products/navbar/cacloaisanphamkhac/quanlyboitron.png"
                                alt=""class="center" width="150" height="150">
                        </div>
                    </a>
                </div>
                <div class="clearfix"></div>
                <br>
                <h5 class="less-mar1 roboto-slab"><a href="/cata-mo">Catalogue mỡ bôi trơn SKF</a>
                </h5>
                <p></p>
                <h5 class="text-red-4"></h5>
                <br>
                {{-- <a class="btn btn-red-4 btn-small" href="#"></a> --}}
            </div>
            <!--end item-->

            <div class="col-md-3 col-sm-6 bmargin">
                <div class="shop-product-holder">
                    <a href="/cata-truyendong">
                        <div class="image-holder">

                            <img src="/assets/products/truyendong/daydai.png"
                                alt=""class="center" width="150" height="150">
                        </div>
                    </a>
                </div>
                <div class="clearfix"></div>
                <br>
                <h5 class="less-mar1 roboto-slab"><a href="/cata-truyendong">Catalogue sản phẩm truyền động SKF</a>
                </h5>
                <p></p>
                <h5 class="text-red-4"></h5>
                <br>
            </div>
            <!--end item-->

            <div class="col-md-3 col-sm-6 bmargin">
                <div class="shop-product-holder">
                    <a href="/cata-phot">
                        <div class="image-holder">

                            <img src="/assets/products/photcongnghiep/photthuyluc.png"
                                alt=""class="center" width="150" height="150">
                        </div>
                    </a>
                </div>
                <div class="clearfix"></div>
                <br>
                <h5 class="less-mar1 roboto-slab"><a href="/cata-phot">Catalogue phớt SKF</a>
                </h5>
                <p> </p>
                <h5 class="text-red-4"></h5>
                <br>

            </div>
            <!--end item-->
            <div class="col-md-3 col-sm-6 bmargin">
                <div class="shop-product-holder">
                    <a href="/cata-dungcu">
                        <div class="image-holder">

                            <img src="/assets/products/baotri/dccokhi.png"
                                alt=""class="center" width="150" height="150">
                        </div>
                    </a>
                </div>
                <div class="clearfix"></div>
                <br>
                <h5 class="less-mar1 roboto-slab"><a href="/cata-dungcu">Catalogue dụng cụ bảo trì SKF</a>
                </h5>
                <p> </p>
                <h5 class="text-red-4"></h5>
                <br>

            </div>
            <!--end item-->

            <div class="col-md-3 col-sm-6 bmargin">
                <div class="shop-product-holder">
                    <a href="/cata-olan">
                        <div class="image-holder">

                            <img src="/assets/products/olan.png"
                                alt=""class="center" width="150" height="150">
                        </div>
                    </a>
                </div>
                <div class="clearfix"></div>
                <br>
                <h5 class="less-mar1 roboto-slab"><a href="/cata-olan">Catalogue ổ lăn SKF</a>
                </h5>
                <p> </p>
                <h5 class="text-red-4"></h5>
                <br>

            </div>
            <!--end item-->
        </div>
    </div>
</section>
@endsection
