@extends('master')
@section('content')
<div class="blog_content">
    <h3 class="post_title" style="text-align:center">Phần mềm kiểm tra hàng giả, hàng nhái SKF</h3>
    <br>
    <br>
    {{-- <div class="post_meta">
        <span><i class="fa fa-calendar" aria-hidden="true"></i> Đăng ngày 04/06/2018 13:52</span> --}}
    </div>
    <div class="clear"></div>
    <div class="post_content" style="text-align:left">
        <div style="text-align:center"><span style="font-size: 30px; font-family: Open Sans, sans-serif;">Kính gửi Quý khách hàng</span></div>
        <br>
        <br>
<div style="padding-left: 22px"><span style="font-size: 21px; font-family: Open Sans, sans-serif;">Hiện nay trên thị trường xuất hiện rất nhiều hàng giả hàng nhái kém chất lượng, để đảm bảo hàng hóa cấp cho khách hàng&nbsp;bảo chất lượng chúng tôi đề nghị Quý công ty cung cấp cho chung tôi các giấy tờ sau:</span></div>

<br>
<div style="padding-left: 22px"><span style="font-size: 21px; font-family: Open Sans, sans-serif;">1. Giấy phép kinh doanh (bản sao)</span></div>
<br>
<div style="padding-left: 22px"><span style="font-size: 21px; font-family: Open Sans, sans-serif;">2. CO&amp;CQ (bản gốc có đóng dấu của quý công ty)</span></div>
<br>
<div style="padding-left: 22px"><span style="font-size: 21px; font-family: Open Sans, sans-serif;">3. Giấy phép bán hàng hoặc ủy quyền bán hàng, hợp đồng đại lý với nhà sản xuất</span></div>
<br>
<div style="padding-left: 22px"><span style="font-size: 21px; font-family: Open Sans, sans-serif;">3. Phương pháp kiểm tra hàng thật hàng giản đối với sản phẩm mình cung cấp</span></div>
<div style="padding-left: 22px"><span style="font-size: 21px; font-family: Open Sans, sans-serif;">ví dụ với hãng SKF có phương pháp sau:</span></div>
<br>
<div style="padding-left: 22px"><span style="font-size: 21px; font-family: Open Sans, sans-serif;">Ứng dụng này được sử dụng trên điện thoại thông minh (Android/ IOS), cho phép người sự dụng chụp hình các sản phẩm nghi ngờ cho SKF để SKF kiểm tra.&nbsp;</span><br><br><span style="font-size: 21px; font-family: Open Sans, sans-serif;">Ứng dụng này có thể tải hoàn toàn MIỄN PHÍ trên Apple Store hoặc Google Play.&nbsp;</span></div>
<div>
<br>
<br>
<div style="padding-left: 22px"><span style="font-size: 21px; font-family: Open Sans, sans-serif;">Video hướng dẫn sử dụng phần mềm.</span></div>
<div style="padding-left: 22px"><span style="font-size: 21px; font-family: Open Sans, sans-serif;"><a href="https://www.youtube.com/watch?v=QPRiSJeqsrg" target="_blank" data-saferedirecturl="https://www.google.com/url?hl=vi&amp;q=https://www.youtube.com/watch?v%3DQPRiSJeqsrg&amp;source=gmail&amp;ust=1528180945650000&amp;usg=AFQjCNEcpRr2nS4WhGVUZVYNEgZTHtrCpw">https://www.youtube.com/watch?v=QPRiSJeqsrg</a></span></div>
<div style="padding-left: 22px"><span style="font-size: 21px; font-family: Open Sans, sans-serif;"><br></span></div>
<div>&nbsp;</div>
</div>
<div style="padding-left: 22px"><span style="font-size: 21px; font-family: Open Sans, sans-serif;">Anh có thể theo link sau để tìm hiểu về ứng dụng:&nbsp;</span><br><span style="font-size: 21px; font-family: Open Sans, sans-serif;"><a href="https://play.google.com/store/apps/details?id=com.skf.authenticate&amp;hl=vi" target="_blank" data-saferedirecturl="https://www.google.com/url?hl=vi&amp;q=https://play.google.com/store/apps/details?id%3Dcom.skf.authenticate%26hl%3Dvi&amp;source=gmail&amp;ust=1528180945650000&amp;usg=AFQjCNGk50RmA0HJBHHW9pqGMo6pJjx4tw">https://play.google.com/store/apps/details?id=com.skf.authenticate&amp;hl=vi</a>&nbsp;</span></div>
<div>&nbsp;</div>
<div style="padding-left: 22px"><br><br><span style="font-size: 21px; font-family: Open Sans, sans-serif;">Các bước thực hiện:&nbsp;</span><br><br><span style="font-size: 21px; font-family: Open Sans, sans-serif;"><em><span style="text-decoration: underline;">Bước 1</span></em>: Chụp ảnh tổng thể bao bì đóng gói (vỏ hộp, số serial...) của vòng bi.&nbsp;</span><br><br><span style="font-size: 21px; font-family: Open Sans, sans-serif;"><em><span style="text-decoration: underline;">Bước 2</span></em>: Gửi ảnh vòng bi và ghi thông tin liên hệ rồi gửi đi.&nbsp;</span><br><br><span style="font-size: 21px; font-family: Open Sans, sans-serif;">Tất cả các thông tin này sẽ được cập nhật ngay lập tức tới SKF.&nbsp;</span><br><span style="font-size: 21px; font-family: Open Sans, sans-serif;">SKF sẽ kiểm tra và thông báo lại kết quả tới anh trong thời gian sớm nhất.&nbsp;</span></div>
<div style="padding-left: 22px"><span style="font-size: 21px; font-family: Open Sans, sans-serif;"><br></span></div>
<div style="padding-left: 22px"><span style="font-size: 21px; font-family: Open Sans, sans-serif;"><img src="http://file.talaweb.com/u1056948/home/%E1%BA%A2nh/au.PNG" alt=""></span></div>
<br>
<br>
<div style="padding-left: 22px"><b>Mời bạn xem video kèm theo </b></div>
                <div align="center"><iframe width="500" height="380" src="https://www.youtube.com/embed/QPRiSJeqsrg" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen=""></iframe></div>                        </div>
</div>
@endsection
