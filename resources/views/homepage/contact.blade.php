@extends('master')
@section('content')
    <section class="sec-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-8">

                    <div class="smart-forms bmargin">
                        <h3 class=" roboto-slab">Liên hệ</h3>
                        <p>Cảm ơn bạn đã quan tâm tới sản phẩm SKF chính hãng. Nếu bạn có bất kỳ thắc mắc nào về sản phẩm
                            SKF chính hãng. Hãy liên hệ với chúng tôi.</p>
                        <br>
                        Đại lý SKF chính hãng tại Việt Nam
                        Add: 04 Trần Kế Xế Xương, Tổ 10, Phường Hải Châu II, Quận Hải Châu, Thành phố Đà Nẵng, Việt Nam
                        <br>
                        <br>
                        <a href="tel:+840982488345"><li><i class="fa fa-phone"></i> Phone 1: 0982488345 </li></a>
                        <br>
                        <a href="tel:+840913665565"><li><i class="fa fa-phone"></i> Phone 2: 0913665565 </li></a>
                        <br>
                        <a href="tel:+840889041468"><li><i class="fa fa-phone"></i> Phone 3: 0889041468 </li></a>
                        <br>
                        <a href="mailto: mtc.skfdanang@gmail.com"><li><i class="fa fa-envelope"></i> Email: mtc.skfdanang@gmail.com </li></a>
                        <br>
                        <br>

                    </div>

                </div>
                <!--end left-->

                <div class="col-md-4 bmargin">
                    {{-- <h3 class="">Thông tin liên hệ</h3>

                    <h6><strong>MTC COMPANY</strong></h6>04 Trần Kế Xương, thành phố Đà Nẵng, Việt Nam <br>
                    <br>
                    <br>
                    <a href="tel:+840982488345"><li><i class="fa fa-phone"></i> Phone 1: 0982488345 </li></a>
                    <br>
                    <a href="tel:+840913665565"><li><i class="fa fa-phone"></i> Phone 2: 0913665565 </li></a>
                    <br>
                    <a href="tel:+840889041468"><li><i class="fa fa-phone"></i> Phone 3: 0889041468 </li></a>
                    <br><br>

                    <br>
                    E-mail: <a href="mailto:mtc.skfdanang@gmail.com">mtc.skfdanang@gmail.com</a><br>
                    Website: <a href="/www.vongbimtc.com">www.vongbimtc.com</a>
                    <div class="clearfix"></div>
                    <br> --}}
                    <h3 class="love-ya-like-a-sister">Tìm địa chỉ</h3>
                    <iframe
                        src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3833.9640660777372!2d108.21529691518701!3d16.067354388882173!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x314218345659e309%3A0x92695eb6e98f5b0e!2zMDQgVHLhuqduIEvhur8gWMawxqFuZywgSOG6o2kgQ2jDonUgMiwgSOG6o2kgQ2jDonUsIMSQw6AgTuG6tW5nIDU1MDAwMCwgVmnhu4d0IE5hbQ!5e0!3m2!1svi!2s!4v1661460472145!5m2!1svi!2s"
                        width="450" height="320" style="border:0;" allowfullscreen="" loading="lazy"
                        referrerpolicy="no-referrer-when-downgrade"></iframe>
                </div>
                <!--end right-->
            </div>
        </div>
    </section>
    <!-- end site wraper -->
@endsection
