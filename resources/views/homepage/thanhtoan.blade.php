@extends('master')
@section('content')
<div class="about_section">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-12">
                <div class="about_content">
                    <p><strong><span style="font-size: medium;">SKF MTC Việt Nam áp dụng phương thức thanh toán linh hoạt cho Khách hàng cụ thể như sau:</span></strong></p>
<p><span style="font-size: medium;">1. Giao hàng và thu tiền tận địa chỉ của Khách hàng.</span></p>
<p><span style="font-size: medium;">2. Thu tiền mặt tại văn phòng MTC ĐÀ NẴNG.</span></p>
<p><span style="font-size: medium;">3. Chuyển khoản qua tài khoản ngân hàng.</span></p>
<p><br><span style="font-size: medium;"> Liên hệ với chúng tôi nếu bạn chuyển khoản qua ngân hàng</span></p>                </div>
            </div>
        </div>
    </div>
</div>
@endsection
