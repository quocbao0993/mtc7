@extends('master')
@section('content')
    <div class="container">

        <h1 style="font-size:20px ;font-weight:700">Catalogue SP truyền động SKF</h1>


        <br>
        <style>
            .download-list .item {
                float: left;
                width: 50%;
                padding: 15px;
                padding-right: 30px;
            }

            .download-list .item.col2 {
                padding-left: 30px;
                padding-right: 15px;
            }

            .download-list .item .inner {
                border-bottom: 1px dotted #ccc;
                position: relative;
                padding-right: 50px;
                min-height: 56px;
            }

            .download-list .item .inner .tt {
                padding: 0 0 5px;
                margin: 0;
                font-size: 16px;
                display: block;
                white-space: nowrap;
                overflow: hidden;
                text-overflow: ellipsis;
                font-weight: 400;
            }

            .download-list .item .inner .btn {
                position: absolute;
                right: 0;
            }

            .download-list .item .inner p {
                display: block;
                white-space: nowrap;
                overflow: hidden;
                text-overflow: ellipsis;
            }

            @media (max-width: 991px) {
                .download-list .item {
                    width: auto;
                    float: none;
                    padding-right: 15px;
                }

                .download-list .item.col2 {
                    padding-left: 15px;
                }
            }
        </style>

        <div style="margin-top: -50px">
            <div class="page-header"></div>
            <div class="download-list row">

                <div class="item">
                    <div class="inner">
                        <a title="Tải xuống SKF Transmission Chains" target="_blank"
                            href="/assets/catalogue/truyendong/SKF-Transmission-Chains.pdf" class="btn btn-info"><i
                                class="fa fa-download"></i></a>

                        <h2 class="tt"><a title="Tải xuống SKF Transmission Chains" target="_blank"
                                href="/assets/catalogue/truyendong/SKF-Transmission-Chains.pdf">SKF Transmission Chains</a></h2>
                    </div>
                </div>
                <div class="item">
                    <div class="inner">
                        <a title="Tải xuống Tài liệu Đĩa Xích ANSI" target="_blank"
                            href="/assets/catalogue/truyendong/Dia_xich_ANSI.pdf" class="btn btn-info"><i
                                class="fa fa-download"></i></a>

                        <h2 class="tt"><a title="Tải xuống Tài liệu Đĩa Xích ANSI" target="_blank"
                                href="/assets/catalogue/truyendong/Dia_xich_ANSI.pdf">Tài liệu Đĩa Xích ANSI</a></h2>
                        <p></p>
                    </div>
                </div>
                <div class="item">
                    <div class="inner">
                        <a title="Tải xuống Tài liệu về Xích SKF" target="_blank" href="/assets/catalogue/truyendong/Xich.pdf"
                            class="btn btn-info"><i class="fa fa-download"></i></a>

                        <h2 class="tt"><a title="Tải xuống Tài liệu về Xích SKF" target="_blank"
                                href="/assets/catalogue/truyendong/Xich.pdf">Tài liệu về Xích SKF</a></h2>
                        <p></p>
                    </div>
                </div>
                <div class="item">
                    <div class="inner">
                        <a title="Tải xuống Tài liệu Đĩa Xích hệ Mét" target="_blank"
                            href="/assets/catalogue/truyendong/Dia_xich_m.pdf" class="btn btn-info"><i
                                class="fa fa-download"></i></a>

                        <h2 class="tt"><a title="Tải xuống Tài liệu Đĩa Xích hệ Mét" target="_blank"
                                href="/assets/catalogue/truyendong/Dia_xich_m.pdf">Tài liệu Đĩa Xích hệ Mét</a></h2>
                        <p></p>
                    </div>
                </div>
                <div class="item">
                    <div class="inner">
                        <a title="Tải xuống Catalogue sản phẩm chuyển động SKF" target="_blank"
                            href="/assets/catalogue/truyendong/san-pham-chuyen-dong-skf.pdf" class="btn btn-info"><i
                                class="fa fa-download"></i></a>

                        <h2 class="tt"><a title="Tải xuống Catalogue sản phẩm chuyển động SKF" target="_blank"
                                href="/assets/catalogue/truyendong/san-pham-chuyen-dong-skf.pdf">Catalogue sản phẩm chuyển động
                                SKF</a></h2>
                        <p></p>
                    </div>
                </div>
                <div class="item">
                    <div class="inner">
                        <a title="Tải xuống Catalogue dây đai SKF" target="_blank"
                            href="/assets/catalogue/truyendong/day-dai-SKF.pdf" class="btn btn-info"><i
                                class="fa fa-download"></i></a>

                        <h2 class="tt"><a title="Tải xuống Catalogue dây đai SKF" target="_blank"
                                href="/assets/catalogue/truyendong/day-dai-SKF.pdf">Catalogue dây đai SKF</a></h2>
                        <p></p>
                    </div>
                </div>

                <div class="clearfix"></div>

            </div>
        </div>


    </div>
@endsection
