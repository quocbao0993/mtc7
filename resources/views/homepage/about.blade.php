@extends('master')
@section('content')

<section class="sec-padding">
    <div class="container">
    <div class="row">

     <div class="col-md-9 col-centered text-center">
            <h5 class="roboto-slab text-red-4 font-weight-3">SKF MTC là đại lý ủy quyền vòng bi SKF chính hãng tại Việt Nam. Được thành lập bởi đội ngũ quản lý với nhiều năm kinh nghiệm trong lĩnh vực kinh doanh vòng bi và các sản phẩm công nghiệp, đã tham gia nhiều dự án lớn trong nhiều lĩnh vực, ngành nghề khác nhau. Thành quả đạt được là sự tín nhiệm của tập đoàn SKF, hãng sản xuất vòng bi lớn nhất thế giới, và trở thành nhà phân phối ủy quyền sản phẩm SKF chính hãng tại Việt Nam.  </h5>
          </div>
          <br><br>
          <div class="divider-line solid light"></div>
          <br>
          <br>

    <div class="col-md-4 bmargin">
    <img src="https://www.skf.com/binaries/pub12/Images/0901d196804deece-TIH_L33MB_HEA0101V_tcm_12-551395.png" alt="" class="img-responsive">
    <br>
    <h5 class="roboto-slab">SỨ MỆNH</h5>
    <p>SKF MTC nỗ lực mang lại giá trị và sự thịnh vượng cho Khách hàng khi sử dụng các sản phẩm SKF chính hãng</p>
    </div>
    <!--end item-->

    <div class="col-md-4 bmargin">
    <img src="https://www.skf.com/binaries/pub12/Images/0901d19680c14a3d-SKF-Bearing-Life-Cycle-Campaign_tcm_12-572724.png" alt="" class="img-responsive">
    <br>
    <h5 class="roboto-slab">GIÁ TRỊ CỐT LÕI</h5>
    <p> SKF MTC luôn cố gắng chuẩn bị đầy đủ năng lực thực thi, nỗ lực hết mình để đảm bảo đúng và cao hơn các cam kết của mình với Khách hàng, đặc biệt là các cam kết về chất lượng sản phẩm – dịch vụ và tiến độ giao hàng.</p>
    </div>
    <!--end item-->

    <div class="col-md-4 bmargin">
    <img src="https://www.skf.com/binaries/pub12/Images/0901d196809bc9d5-High-speed-passenger-train-shutterstock_551949058_tcm_12-541487.png" alt="" class="img-responsive">
    <br>
    <h5 class="roboto-slab">TẦM NHÌN</h5>
    <p>SKF MTC nỗ lực phấn đấu trở thành nhà cung cấp các sản phẩm SKF chính hãng hàng đầu tại Việt Nam. </p>
    </div>
    <!--end item-->


    </div>
    </div>
    </section>
    <div class="choseus_area">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="chose_title text-center">
                        <h1>Tại sao khách hàng chọn chúng tôi?</h1>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6">
                    <div class="single_chose">
                        <div class="chose_icone" style="text-align:center">
                            <img src="https://tstvietnam.vn/skins/customize/assets/img/about/About_icon1.jpg" alt="">
                        </div>
                        <div class="chose_content" style="text-align:center">
                            <h3>Sản phẩm chính hãng</h3>
                            <p>Sản phẩm nhập khẩu chính hãng qua SKF Việt Nam. Chúng tôi cam kết hoàn tiền gấp 100 lần giá trị đơn hàng nếu phát hiện hàng giả, hàng nhái </p>

                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6">
                    <div class="single_chose">
                        <div class="chose_icone" style="text-align:center">
                            <img src="https://tstvietnam.vn/skins/customize/assets/img/about/About_icon3.jpg" alt="">
                        </div>
                        <div class="chose_content" style="text-align:center">
                            <h3>Giao hàng toàn quốc</h3>
                            <p>Cho dù bạn ở đâu? chúng tôi luôn sẵn sàng được phục vụ nhu cầu của bạn với chế độ giao hàng và thanh toán linh hoạt nhất</p>

                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6">
                    <div class="single_chose">
                        <div class="chose_icone" style="text-align:center">
                            <img src="https://tstvietnam.vn/skins/customize/assets/img/about/About_icon3.jpg" alt="">
                        </div>
                        <div class="chose_content" style="text-align:center">
                            <h3>Hỗ trợ kỹ thuật 24/7</h3>
                            <p>Đội ngũ bán hàng &amp; kỹ thuật của chúng tôi luôn sẵn sàng phục vụ và hỗ trợ bạn mọi lúc mọi nơi tất cả các ngày trong tuần 24/24</p>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


  <div class="clearfix"></div>
@endsection
