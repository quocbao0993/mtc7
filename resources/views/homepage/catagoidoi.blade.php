@extends('master')
@section('content')
    <div class="container">

        <h1 style="font-size:20px ;font-weight:700">Catalogue gối đỡ SKF</h1>


        <br>
        <style>
            .download-list .item {
                float: left;
                width: 50%;
                padding: 15px;
                padding-right: 30px;
            }

            .download-list .item.col2 {
                padding-left: 30px;
                padding-right: 15px;
            }

            .download-list .item .inner {
                border-bottom: 1px dotted #ccc;
                position: relative;
                padding-right: 50px;
                min-height: 56px;
            }

            .download-list .item .inner .tt {
                padding: 0 0 5px;
                margin: 0;
                font-size: 16px;
                display: block;
                white-space: nowrap;
                overflow: hidden;
                text-overflow: ellipsis;
                font-weight: 400;
            }

            .download-list .item .inner .btn {
                position: absolute;
                right: 0;
            }

            .download-list .item .inner p {
                display: block;
                white-space: nowrap;
                overflow: hidden;
                text-overflow: ellipsis;
            }

            @media (max-width: 991px) {
                .download-list .item {
                    width: auto;
                    float: none;
                    padding-right: 15px;
                }

                .download-list .item.col2 {
                    padding-left: 15px;
                }
            }
        </style>

        <div style="margin-top: -50px">
            <div class="page-header"></div>
            <div class="download-list row">

                <div class="item">
                    <div class="inner">
                        <a title="Tải xuống Thông số gối đỡ UC seri 200 - 300" target="_blank"
                            href="/assets/catalogue/goido/UC200-300.pdf" class="btn btn-info"><i
                                class="fa fa-download"></i></a>

                        <h2 class="tt"><a title="Tải xuống Thông số gối đỡ UC seri 200 - 300" target="_blank"
                                href="/assets/catalogue/goido/UC200-300.pdf">Thông số gối đỡ UC seri 200 - 300</a></h2>
                        <p></p>
                    </div>
                </div>
                <div class="item">
                    <div class="inner">
                        <a title="Tải xuống Tài liệu các loại gối UC SKF" target="_blank"
                            href="/assets/catalogue/goido/thong-so-goi-UC-SKF.pdf" class="btn btn-info"><i
                                class="fa fa-download"></i></a>

                        <h2 class="tt"><a title="Tải xuống Tài liệu các loại gối UC SKF" target="_blank"
                                href="/assets/catalogue/goido/thong-so-goi-UC-SKF.pdf">Tài liệu các loại gối UC SKF</a></h2>
                        <p></p>
                    </div>
                </div>
                <div class="item">
                    <div class="inner">
                        <a title="Tải xuống Tài liệu kỹ thuật tổng hợp về Gối SKF" target="_blank"
                            href="/assets/catalogue/goido/SKF bearing housings and roller bearing unit.pdf"
                            class="btn btn-info"><i class="fa fa-download"></i></a>

                        <h2 class="tt"><a title="Tải xuống Tài liệu kỹ thuật tổng hợp về Gối SKF" target="_blank"
                                href="/assets/catalogue/goido/SKF bearing housings and roller bearing unit.pdf">Tài liệu kỹ
                                thuật tổng hợp về Gối SKF</a></h2>
                        <p></p>
                    </div>
                </div>

                <div class="clearfix"></div>

            </div>
        </div>


    </div>
@endsection
