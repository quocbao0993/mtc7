@extends('master')
@section('content')
<div class="master-slider ms-skin-default" id="masterslider">

    <!-- slide 1 -->
    <div class="ms-slide slide-1" data-delay="9">
      <div class="slide-pattern"></div>

      <img src="/assets/homepage/slide1.png"/>

      <h3 class="ms-layer text35"
			style="top: 180px; left:230px;"
			data-type="text"
			data-delay="500"
			data-ease="easeOutExpo"
			data-duration="1230"
			data-effect="scale(1.5,1.6)">UY TÍN</h3>

      <h3 class="ms-layer text35"
			style="top:240px; left:230px;"
			data-type="text"
			data-delay="1000"
			data-ease="easeOutExpo"
			data-duration="1230"
			data-effect="scale(1.5,1.6)"> CHẤT LƯỢNG</h3>

      <h3 class="ms-layer text3"
			style="top: 310px; left:230px;"
			data-type="text"
			data-delay="1500"
			data-ease="easeOutExpo"
			data-duration="1230"
			data-effect="scale(1.5,1.6)"> CUNG CẤP VÒNG BI SKF
        <br/>
        <br>
        CHÍNH HÃNG TẠI VIỆT NAM
        </h3>

      {{-- <a class="ms-layer sbut15"
			style="left: 230px; top: 380px;"
			data-type="text"
			data-delay="2000"
			data-ease="easeOutExpo"
			data-duration="1200"
			data-effect="scale(1.5,1.6)"> Read more </a>

      <div class="ms-layer offer-badge"
			style="left:700px; top:200px"
		 	data-effect="bottom(150)"
		 	data-duration="2500"
		 	data-ease="easeOutExpo"
		><br/>
        60%<br/>
        Off<br/>
        Sales</div> --}}

    </div>
    <!-- end slide 1 -->


    <!-- slide 2 -->
    <div class="ms-slide slide-2" data-delay="9">
      <div class="slide-pattern"></div>

      <img src="/assets/homepage/slide2.png" >

      <h3 class="ms-layer text35"
			style="top: 180px; left:230px;"
			data-type="text"
			data-delay="500"
			data-ease="easeOutExpo"
			data-duration="1230"
			data-effect="left(250)"> MÁY GIA NHIỆT SKF</h3>

      <h3 class="ms-layer text35"
			style="top:240px; left:230px;"
			data-type="text"
			data-delay="1000"
			data-ease="easeOutExpo"
			data-duration="1230"
			data-effect="right(250)"> GIA NHIỆT CHO VÒNG BI</h3>

      <h3 class="ms-layer text3"
			style="top: 310px; left:230px;"
			data-type="text"
			data-delay="1500"
			data-ease="easeOutExpo"
			data-duration="1230"
			data-effect="scale(1.5,1.6)"> TỪ NHỎ ĐẾN LỚN
       <br/>
       </h3>

      {{-- <a class="ms-layer sbut15"
			style="left: 230px; top: 380px;"
			data-type="text"
			data-delay="2000"
			data-ease="easeOutExpo"
			data-duration="1200"
			data-effect="scale(1.5,1.6)"> Read more </a>

      <div class="ms-layer offer-badge"
			style="left:700px; top:200px"
		 	data-effect="bottom(150)"
		 	data-duration="2500"
		 	data-ease="easeOutExpo"
		><br/>
        60%<br/>
        Off<br/>
        Sales</div> --}}

    </div>
    <!-- end slide 2 -->


    <!-- slide 3 -->
    <div class="ms-slide slide-3" data-delay="9">
      <div class="slide-pattern"></div>

      <img src="/assets/homepage/slide3.png"/>

      <h3 class="ms-layer text35"
			style="top: 180px; left:230px;"
			data-type="text"
			data-delay="500"
			data-ease="easeOutExpo"
			data-duration="1230"
			data-effect="left(250)"> DỤNG CỤ BẢO TRÌ</h3>

      <h3 class="ms-layer text35"
			style="top:240px; left:230px;"
			data-type="text"
			data-delay="1000"
			data-ease="easeOutExpo"
			data-duration="1230"
			data-effect="right(250)"> VÀ CÁC LOẠI</h3>

      <h3 class="ms-layer text3"
			style="top: 310px; left:230px;"
			data-type="text"
			data-delay="1500"
			data-ease="easeOutExpo"
			data-duration="1230"
			data-effect="scale(1.5,1.6)">THIẾT BỊ GIÁM SÁT SKF
        <br/>
       </h3>
{{--
      <a class="ms-layer sbut15"
			style="left: 230px; top: 380px;"
			data-type="text"
			data-delay="2000"
			data-ease="easeOutExpo"
			data-duration="1200"
			data-effect="scale(1.5,1.6)"> Read more </a>

      <div class="ms-layer offer-badge"
			style="left:700px; top:200px"
		 	data-effect="bottom(150)"
		 	data-duration="2500"
		 	data-ease="easeOutExpo"
		><br/>
        60%<br/>
        Off<br/>
        Sales</div> --}}

    </div>
    <!-- end slide 3 -->

  </div>
  <!-- end of masterslider -->
  <div class="clearfix"></div>
<section class="parallax-section53">
    <div class="section-overlay bg-opacity-1">
      <div class="container sec-tpadding-3 sec-bpadding-3">
        <div class="row">
          <div class="col-md-8 col-centered">
            <h1 class="text-white parallax-section-title uppercase roboto-slab font-weight-6 less-mar1">SKF MTC - ĐẠI LÝ ỦY QUYỀN VÒNG BI SKF CHÍNH HÃNG TẠI VIỆT NAM</h1>
            <h4 class="parallax-section-title-2 uppercase text-white"></h4>
            <br>
            <p class="text-white">Với phương châm phát triển bền vững, lộ trình hợp tác rộng khắp. SKF MTC nỗ lực mang lại giá trị và sự thịnh vượng cho Khách hàng khi sử dụng các sản phẩm SKF chính hãng.</p>
            <br>
          </div>
        </div>
      </div>
    </div>
  </section>
    <!-- end section -->
<div class="clearfix"></div>
    <section class="sec-padding">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 ">
                    <h4 class="section-title-7"><span class="roboto-slab uppercase">SẢN PHẨM SKF CÁC LOẠI CHÍNH HÃNG</span></h4>
                </div>
                <!--end title-->

                <div class="col-md-4 col-sm-6 bmargin">
                    <div class="shop-product-holder">
                        <a href="/o-bi">
                            <div class="image-holder">

                                <img src="/assets/products/navbar/obi/obi.png"
                                    alt=""  class="center" width="150" height="150">
                            </div>
                        </a>
                    </div>
                    <div class="clearfix"></div>
                    <br>
                    <h5 class="less-mar1 roboto-slab"><a href="/o-bi">Ổ BI CÁC LOẠI SKF CHÍNH HÃNG</a>
                    </h5>
                    <p></p>
                    <h5 class="text-red-4"></h5>
                    <br>
                    {{-- <a class="btn btn-red-4 btn-small" href="#"></a> --}}
                </div>
                <!--end item-->

                <div class="col-md-4 col-sm-6 bmargin">
                    <div class="shop-product-holder">
                        <a href="/cum-goi-doi-o-lan">
                            <div class="image-holder">

                                <img src="/assets/products/navbar/goidovaolan/cumolan.png"
                                    class="center" width="150" height="150">
                            </div>
                        </a>
                    </div>
                    <div class="clearfix"></div>
                    <br>
                    <h5 class="less-mar1 roboto-slab"><a href="">CỤM GỐI ĐỠ VÀ Ổ LĂN</a>
                    </h5>
                    <p> </p>
                    <h5 class="text-red-4"></h5>
                    <br>
                    {{-- <a class="btn btn-red-4 btn-small" href="#"></a> --}}
                </div>
                <!--end item-->

                <div class="col-md-4 col-sm-6 bmargin">
                    <div class="shop-product-holder">
                        <a href="/vong-bi-chinh-xac">
                            <div class="image-holder">

                                <img src="/assets/products/navbar/vongbisieuchinhxac/obitiepxucgoc.jfif"
    class="center" width="150" height="150">
                            </div>
                        </a>
                    </div>
                    <div class="clearfix"></div>
                    <br>
                    <h5 class="less-mar1 roboto-slab"><a href="/vong-bi-chinh-xac">VÒNG BI SIÊU CHÍNH XÁC</a>
                    </h5>
                    <p></p>
                    <h5 class="text-red-4"></h5>
                    <br>
                    {{-- <a class="btn btn-red-4 btn-small" href="#"></a> --}}
                </div>
                <!--end item-->

                <div class="col-md-4 col-sm-6 bmargin">
                    <div class="shop-product-holder">
                        <a href="/o-vanh-xoay">
                            <div class="image-holder">

                                <img src="/assets/products/navbar/ovanhxoay/4diem.png"
    class="center" width="150" height="150">
                            </div>
                        </a>
                    </div>
                    <div class="clearfix"></div>
                    <br>
                    <h5 class="less-mar1 roboto-slab"><a href="/o-vanh-xoay">Ổ VÀNH XOAY</a>
                    </h5>
                    <p></p>
                    <h5 class="text-red-4"></h5>
                    <br>
                </div>
                <!--end item-->

                <div class="col-md-4 col-sm-6 bmargin">
                    <div class="shop-product-holder">
                        <a href="/o-truot">
                            <div class="image-holder">

                                <img src="/assets/products/navbar/otruot/otruottangtrong.png"
    class="center" width="150" height="150">
                            </div>
                        </a>
                    </div>
                    <div class="clearfix"></div>
                    <br>
                    <h5 class="less-mar1 roboto-slab"><a href="/o-truot">Ổ TRƯỢT</a>
                    </h5>
                    <p> </p>
                    <h5 class="text-red-4"></h5>
                    <br>

                </div>
                <!--end item-->
                <div class="col-md-4 col-sm-6 bmargin">
                    <div class="shop-product-holder">
                        <a href="">
                            <div class="image-holder">

                                <img src="/assets/products/navbar/cacloaisanphamkhac/vongbitutinh.png"
    class="center" width="150" height="150">
                            </div>
                        </a>
                    </div>
                    <div class="clearfix"></div>
                    <br>
                    <h5 class="less-mar1 roboto-slab"><a href="">CÁC HỆ THỐNG VÀ VÒNG BI TỪ TÍNH</a>
                    </h5>
                    <p> </p>
                    <h5 class="text-red-4"></h5>
                    <br>

                </div>
                <div class="col-md-4 col-sm-6 bmargin">
                    <div class="shop-product-holder">
                        <a href="/phot-chan-dau">
                            <div class="image-holder">

                                <img src="/assets/products/navbar/cacloaisanphamkhac/photdaucongnghiep.png"
    class="center" width="150" height="150">
                            </div>
                        </a>
                    </div>
                    <div class="clearfix"></div>
                    <br>
                    <h5 class="less-mar1 roboto-slab"><a href="/phot-chan-dau">PHỚT CHẶN DẦU CHUYÊN NGHIỆP</a>
                    </h5>
                    <p> </p>
                    <h5 class="text-red-4"></h5>
                    <br>

                </div>
                <div class="col-md-4 col-sm-6 bmargin">
                    <div class="shop-product-holder">
                        <a href="/quan-ly-boi-tron">
                            <div class="image-holder">

                                <img src="/assets/products/navbar/cacloaisanphamkhac/quanlyboitron.png"
    class="center" width="150" height="150">
                            </div>
                        </a>
                    </div>
                    <div class="clearfix"></div>
                    <br>
                    <h5 class="less-mar1 roboto-slab"><a href="/quan-ly-boi-tron">QUẢN LÝ BÔI TRƠN</a>
                    </h5>
                    <p> </p>
                    <h5 class="text-red-4"></h5>
                    <br>

                </div>
                <div class="col-md-4 col-sm-6 bmargin">
                    <div class="shop-product-holder">
                        <a href="/san-pham-bao-tri">
                            <div class="image-holder">

                                <img src="/assets/products/navbar/cacloaisanphamkhac/spbaotri.png"
    class="center" width="150" height="150">
                            </div>
                        </a>
                    </div>
                    <div class="clearfix"></div>
                    <br>
                    <h5 class="less-mar1 roboto-slab"><a href="/san-pham-bao-tri">SẢN PHẨM BẢO TRÌ</a>
                    </h5>
                    <p> </p>
                    <h5 class="text-red-4"></h5>
                    <br>

                </div>
                <div class="col-md-4 col-sm-6 bmargin">
                    <div class="shop-product-holder">
                        <a href="/giam-sat">
                            <div class="image-holder">

                                <img src="/assets/products/navbar/cacloaisanphamkhac/giamsat.png"
    class="center" width="150" height="150">
                            </div>
                        </a>
                    </div>
                    <div class="clearfix"></div>
                    <br>
                    <h5 class="less-mar1 roboto-slab"><a href="/giam-sat">HỆ THỐNG GIÁM SÁT TÌNH TRẠNG HOẠT ĐỘNG</a>
                    </h5>
                    <p> </p>
                    <h5 class="text-red-4"></h5>
                    <br>

                </div>
                <div class="col-md-4 col-sm-6 bmargin">
                    <div class="shop-product-holder">
                        <a href="/truyen-dong">
                            <div class="image-holder">

                                <img src="/assets/products/navbar/cacloaisanphamkhac/truyendong.png"
    class="center" width="150" height="150">
                            </div>
                        </a>
                    </div>
                    <div class="clearfix"></div>
                    <br>
                    <h5 class="less-mar1 roboto-slab"><a href="/truyen-dong">CÁC GIẢI PHÁP TRUYỀN ĐỘNG</a>
                    </h5>
                    <p> </p>
                    <h5 class="text-red-4"></h5>
                    <br>

                </div>
                <div class="col-md-4 col-sm-6 bmargin">
                    <div class="shop-product-holder">
                        <a href="/do-kiem-tra">
                            <div class="image-holder">

                                <img src="/assets/products/navbar/cacloaisanphamkhac/dovakiemtra.png"
    class="center" width="150" height="150">
                            </div>
                        </a>
                    </div>
                    <div class="clearfix"></div>
                    <br>
                    <h5 class="less-mar1 roboto-slab"><a href="/do-kiem-tra">THIỆT BỊ ĐO VÀ KIỂM TRA</a>
                    </h5>
                    <p> </p>
                    <h5 class="text-red-4"></h5>
                    <br>

                </div>
            </div>
        </div>
    </section>

    <!--end section-->
    <div class="clearfix"></div>

    <section class="sec-padding">
        <div class="whyBox ">
            <div class="container">
                <div class="text-center">
                    <h2 class="box-heading text-uppercase"><a href="/san-pham/vong-bi-skf"
                            title="Vòng bi SKF chính hãng"><span style="font-weight: 500; color:#000000">Mua vòng bi
                                SKF</span></a> tại sao nên chọn <span style="font-weight: 500;">SKF MTC ĐÀ NẴNG ?</span>
                    </h2>
                </div>

                <div class="list-item row" itemscope="" itemtype="https://schema.org/FAQPage">
                    <div class="col-md-4 _item" itemscope="" itemprop="mainEntity"
                        itemtype="https://schema.org/Question">
                        <div class="icon _icon1"></div>
                        <div class="info" align="justify">
                            <h3 class="title" itemprop="name">★ Uy tín - Tận tâm</h3>
                            <div class="desc" itemscope="" itemprop="acceptedAnswer"
                                itemtype="https://schema.org/Answer">
                                <span itemprop="text">
                                    MTC đặt chữ TÍN lên vị trí hàng đầu, lấy chữ TÍN làm tôn chỉ hoạt động của doanh nghiệp.
                                    Tận tâm nỗ lực hết mình để đảm bảo đúng các cam kết với Khách hàng về chất lượng sản
                                    phẩm
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 _item" itemscope="" itemprop="mainEntity"
                        itemtype="https://schema.org/Question">
                        <div class="icon _icon2"></div>
                        <div class="info" align="justify">
                            <h3 class="title" itemprop="name">★ <a href="/san-pham"
                                    title="Sản phẩm SKF chính hãng"><span style="color:#000000">Sản phẩm SKF chính
                                        hãng</span></a></h3>
                            <div class="desc" itemscope="" itemprop="acceptedAnswer"
                                itemtype="https://schema.org/Answer">
                                <span itemprop="text">
                                    Mọi sản phẩm do MTC phân phối đều được nhập khẩu chính hãng qua SKF Việt Nam. Chúng tôi
                                    cam kết hoàn tiền đơn hàng nếu phát hiện hàng giả, hàng nhái
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 _item" itemscope="" itemprop="mainEntity"
                        itemtype="https://schema.org/Question">
                        <div class="icon _icon3"></div>
                        <div class="info" align="justify">
                            <h3 class="title" itemprop="name">★ <a href="/dai-ly-uy-quyen-vong-bi-skf.html"
                                    title="SKF Authorized Distributor"><span style="color:#000000">Authorized
                                        Distributor</span></a></h3>
                            <div class="desc" itemscope="" itemprop="acceptedAnswer"
                                itemtype="https://schema.org/Answer">
                                <span itemprop="text">
                                    MTC - Đại lý uỷ quyền SKF chính hãng tại Việt Nam. Mọi sản phẩm SKF chúng tôi bán ra đều
                                    có thông tin rõ ràng, minh bạch cũng như các chế độ sau bán hàng
                                    mà chỉ nhà phân phối ủy quyền SKF mới có.
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>

                    <div class="col-md-4 _item" itemscope="" itemprop="mainEntity"
                        itemtype="https://schema.org/Question">
                        <div class="icon _icon4"></div>
                        <div class="info" align="justify">
                            <h3 class="title" itemprop="name">★ Bảo hành chính hãng</h3>
                            <div class="desc" itemscope="" itemprop="acceptedAnswer"
                                itemtype="https://schema.org/Answer">
                                <span itemprop="text">
                                    Sản phẩm được bảo hành chính hãng SKF, Chế độ bảo hành đổi mới
                                    linh hoạt. Hỗ trợ tối đa lợi ích của Khách hàng.
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 _item" itemscope="" itemprop="mainEntity"
                        itemtype="https://schema.org/Question">
                        <div class="icon _icon5"></div>
                        <div class="info" align="justify">
                            <h3 class="title" itemprop="name">★ Hỗ trợ kỹ thuật 24/7</h3>
                            <div class="desc" itemscope="" itemprop="acceptedAnswer"
                                itemtype="https://schema.org/Answer">
                                <span itemprop="text">
                                    Đội ngũ bán hàng và kỹ thuật của chúng tôi luôn sẵn sàng phục vụ và hỗ trợ bạn mọi lúc
                                    mọi nơi tất cả các ngày
                                    trong tuần.
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 _item" itemscope="" itemprop="mainEntity"
                        itemtype="https://schema.org/Question">
                        <div class="icon _icon6"></div>
                        <div class="info" align="justify">
                            <h3 class="title" itemprop="name">★ Giao hàng toàn quốc</h3>
                            <div class="desc" itemscope="" itemprop="acceptedAnswer"
                                itemtype="https://schema.org/Answer">
                                <span itemprop="text">
                                    MTC luôn sẵn sàng được phục vụ nhu cầu của bạn với chế độ giao hàng
                                    và thanh toán linh hoạt.
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- end section -->
    <div class="clearfix"></div>

    <section class="sec-padding section-light">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 text-center">
                    <h3 class="uppercase roboto-slab ">Nhập email để nhận những ưu đãi mới nhất của cửa hàng</h3>
                    {{-- <p class="sub-title">Lorem ipsum dolor sit amet consectetuer adipiscing elit Suspendisse et.</p> --}}
                </div>
                <div class="clearfix"></div>
                <div class="col-md-12">
                    <div class="input_holder">
                        <input class="email_input two" type="search" placeholder="Nhập địa chỉ email của bạn">
                        <input name="submit" value="Đăng Ký" class="email_submit two" type="submit">
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--end section-->
    <div class="clearfix"></div>

    {{-- <section class="sec-moreless-padding">
        <div class="container">
        <div class="row">
            <div class="col-md-12">
            <ul class="clientlogo-list two">
                <li><img src="https://ngocanh.com/public/uploads/images/1364/phot-skf-150x150c.jpg?v=2.5.9" alt=""/></li>
                <li><img src="https://ngocanh.com/public/uploads/images/103/mo-skf-150x150c.jpg?v=2.5.9" alt=""/></li>
                <li><img src="https://ngocanh.com/public/uploads/images/1365/dung-cu-skf-150x150c.jpg?v=2.5.9" alt=""/></li>
                <li><img src="https://ngocanh.com/public/uploads/images/5434/vong-bi-xe-tai-skf-150x150c.jpg?v=2.5.9" alt=""/></li>
                <li class="last"><img src="https://ngocanh.com/public/uploads/images/5432/xich-tai-skf-150x150c.jpg?v=2.5.9" alt=""/></li>
            </ul>
            </div>
        </div>
        </div>
    </section> --}}
    <!--end section -->
    <div class="clearfix"></div>
@endsection
