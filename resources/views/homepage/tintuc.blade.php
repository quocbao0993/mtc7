@extends('master')
@section('content')
    <div componentvisibility="">
        <div class="container">
            <div class="row">
                <div _nghost-dov-c129="" body-text-main="" id="cid-492115" class="body-text-main col-md-8 space-between">
                    <div _ngcontent-dov-c129="" class="body-text-main">
                        <div _ngcontent-dov-c129="" class="body">
                            <rich-text-field _ngcontent-dov-c129="" _nghost-dov-c76="">
                                <div _ngcontent-dov-c76="" class="content text-darker-green-grey">
                                    <div _ngcontent-dov-c76="" id="rich-text-div">
                                        <br>
                                        <br>
                                        <p>Bạn có nghĩ rằng bạn có thể đã mua phải sản phẩm SKF giả?</p>
                                        <p>Ứng dụng SKF Authenticate hướng dẫn cụ thể cách chụp ảnh sản phẩm và tự động gửi
                                            yêu cầu, tất cả gói gọn trong một quy trình. Các chuyên gia phụ trách của SKF
                                            sau đó sẽ kiểm tra thông tin, xác minh xem sản phẩm là sản phẩm chính hãng hay
                                            bị làm giả và báo cho bạn biết.</p>
                                    </div>
                                    <!---->
                                </div>
                                <!---->
                                <!---->
                            </rich-text-field>
                            <!---->
                            <!---->
                        </div>
                        <!---->
                    </div>
                </div>
                <div _nghost-dov-c85="" single-image-main="" id="cid-597636" class="col-md-4 space-between" >
                    <div _ngcontent-dov-c85=""><img  width="350" height="350" _ngcontent-dov-c85=""
                            src="https://www.skf.com/binaries/pub12/Images/094f35961b47a64d-SKF_authenticate_QR_code_tcm_12-597633.png"
                            alt="SKF_authenticate_QR_code.png"> </div>
                    <!---->
                </div>
                <!---->
                <!---->
            </div>
        </div>
        <!---->
        <!---->
    </div>
@endsection
