@extends('master')
@section('content')
<div class="container">

    <h1 style="font-size:20px ;font-weight:700">Catalogue Mỡ bôi trơn SKF</h1>


    <br>
    <style>
        .download-list .item {
            float: left;
            width: 50%;
            padding: 15px;
            padding-right: 30px;
        }

        .download-list .item.col2 {
            padding-left: 30px;
            padding-right: 15px;
        }

        .download-list .item .inner {
            border-bottom: 1px dotted #ccc;
            position: relative;
            padding-right: 50px;
            min-height: 56px;
        }

        .download-list .item .inner .tt {
            padding: 0 0 5px;
            margin: 0;
            font-size: 16px;
            display: block;
            white-space: nowrap;
            overflow: hidden;
            text-overflow: ellipsis;
            font-weight: 400;
        }

        .download-list .item .inner .btn {
            position: absolute;
            right: 0;
        }

        .download-list .item .inner p {
            display: block;
            white-space: nowrap;
            overflow: hidden;
            text-overflow: ellipsis;
        }

        @media (max-width: 991px) {
            .download-list .item {
                width: auto;
                float: none;
                padding-right: 15px;
            }

            .download-list .item.col2 {
                padding-left: 15px;
            }
        }
    </style>

    <div style="margin-top: -50px">
        <div class="page-header"></div>
        <div class="download-list row">

            <div class="item">
                <div class="inner">
                    <a title="Tải xuống Thông số kỹ thuật Mỡ SKF LGLT 2" target="_blank"
                        href="/assets/catalogue/moboitron/SKF-LGLT-2.pdf" class="btn btn-info"><i
                            class="fa fa-download"></i></a>

                    <h2 class="tt"><a title="Tải xuống Thông số kỹ thuật Mỡ SKF LGLT 2" target="_blank"
                            href="/assets/catalogue/moboitron/SKF-LGLT-2.pdf">Thông số kỹ thuật Mỡ SKF LGLT
                            2</a></h2>
                    <p></p>
                </div>
            </div>
            <div class="item">
                <div class="inner">
                    <a title="Tải xuống Thông số kỹ thuật Mỡ SKF LGEV 2" target="_blank"
                        href="/assets/catalogue/moboitron/SKF-LGEV-2.pdf" class="btn btn-info"><i
                            class="fa fa-download"></i></a>

                    <h2 class="tt"><a title="Tải xuống Thông số kỹ thuật Mỡ SKF LGEV 2" target="_blank"
                            href="/assets/catalogue/moboitron/SKF-LGEV-2.pdf">Thông số kỹ thuật Mỡ SKF LGEV
                            2</a></h2>
                    <p></p>
                </div>
            </div>
            <div class="item">
                <div class="inner">
                    <a title="Tải xuống Thông số kỹ thuật Mỡ SKF LGEM 2" target="_blank"
                        href="/assets/catalogue/moboitron/SKF-LGEM-2.pdf" class="btn btn-info"><i
                            class="fa fa-download"></i></a>

                    <h2 class="tt"><a title="Tải xuống Thông số kỹ thuật Mỡ SKF LGEM 2" target="_blank"
                            href="/assets/catalogue/moboitron/SKF-LGEM-2.pdf">Thông số kỹ thuật Mỡ SKF LGEM
                            2</a></h2>
                    <p></p>
                </div>
            </div>
            <div class="item">
                <div class="inner">
                    <a title="Tải xuống Thông số kỹ thuật Mỡ SKF LGFP 2" target="_blank"
                        href="/assets/catalogue/moboitron/SKF-LGFP2.pdf" class="btn btn-info"><i
                            class="fa fa-download"></i></a>

                    <h2 class="tt"><a title="Tải xuống Thông số kỹ thuật Mỡ SKF LGFP 2" target="_blank"
                            href="/assets/catalogue/moboitron/SKF-LGFP2.pdf">Thông số kỹ thuật Mỡ SKF LGFP 2</a>
                    </h2>
                    <p></p>
                </div>
            </div>
            <div class="item">
                <div class="inner">
                    <a title="Tải xuống Thông số kỹ thuật Mỡ SKF LGMT 2" target="_blank"
                        href="/assets/catalogue/moboitron/SKF-LGMT-2.pdf" class="btn btn-info"><i
                            class="fa fa-download"></i></a>

                    <h2 class="tt"><a title="Tải xuống Thông số kỹ thuật Mỡ SKF LGMT 2" target="_blank"
                            href="/assets/catalogue/moboitron/SKF-LGMT-2.pdf">Thông số kỹ thuật Mỡ SKF LGMT
                            2</a></h2>
                    <p></p>
                </div>
            </div>
            <div class="item">
                <div class="inner">
                    <a title="Tải xuống Thông số mỡ bôi trơn SKF" target="_blank"
                        href="/download-catalogue/Thong_so_mo_SKF.pdf" class="btn btn-info"><i
                            class="fa fa-download"></i></a>

                    <h2 class="tt"><a title="Tải xuống Thông số mỡ bôi trơn SKF" target="_blank"
                            href="/download-catalogue/Thong_so_mo_SKF.pdf">Thông số mỡ bôi trơn SKF</a></h2>
                    <p></p>
                </div>
            </div>
            <div class="item">
                <div class="inner">
                    <a title="Tải xuống Dụng cụ cho Mỡ bôi trơn" target="_blank"
                        href="/download-catalogue/Dung_cu_cho_mo_boi_tron_SKF.pdf" class="btn btn-info"><i
                            class="fa fa-download"></i></a>

                    <h2 class="tt"><a title="Tải xuống Dụng cụ cho Mỡ bôi trơn" target="_blank"
                            href="/download-catalogue/Dung_cu_cho_mo_boi_tron_SKF.pdf">Dụng cụ cho Mỡ bôi trơn</a></h2>
                    <p></p>
                </div>
            </div>
            <div class="item">
                <div class="inner">
                    <a title="Tải xuống Lựa chọn mỡ bôi trơn" target="_blank"
                        href="/download-catalogue/Lua chon Mo SKF.pdf" class="btn btn-info"><i
                            class="fa fa-download"></i></a>

                    <h2 class="tt"><a title="Tải xuống Lựa chọn mỡ bôi trơn" target="_blank"
                            href="/download-catalogue/Lua chon Mo SKF.pdf">Lựa chọn mỡ bôi trơn</a></h2>
                    <p></p>
                </div>
            </div>
            <div class="item">
                <div class="inner">
                    <a title="Tải xuống Dụng cụ bảo trì và chất bôi trơn" target="_blank"
                        href="/download-catalogue/dung_cu_bao tri_mo_ SKF.pdf" class="btn btn-info"><i
                            class="fa fa-download"></i></a>

                    <h2 class="tt"><a title="Tải xuống Dụng cụ bảo trì và chất bôi trơn" target="_blank"
                            href="/download-catalogue/dung_cu_bao tri_mo_ SKF.pdf">Dụng cụ bảo trì và chất bôi trơn</a>
                    </h2>
                    <p></p>
                </div>
            </div>
            <div class="item">
                <div class="inner">
                    <a title="Tải xuống Thông số các loại mỡ bôi trơn SKF" target="_blank"
                        href="/assets/catalogue/moboitron/thong-so-mo-boi-tron-SKF.pdf"
                        class="btn btn-info"><i class="fa fa-download"></i></a>

                    <h2 class="tt"><a title="Tải xuống Thông số các loại mỡ bôi trơn SKF" target="_blank"
                            href="/assets/catalogue/moboitron/thong-so-mo-boi-tron-SKF.pdf">Thông số các loại
                            mỡ bôi trơn SKF</a></h2>
                    <p></p>
                </div>
            </div>
            <div class="item">
                <div class="inner">
                    <a title="Tải xuống Lựa chọn mỡ bôi trơn Vòng bi" target="_blank"
                        href="/assets/catalogue/moboitron/lua-chon-mo-boi-tron-vong-bi.pdf"
                        class="btn btn-info"><i class="fa fa-download"></i></a>

                    <h2 class="tt"><a title="Tải xuống Lựa chọn mỡ bôi trơn Vòng bi" target="_blank"
                            href="/assets/catalogue/moboitron/lua-chon-mo-boi-tron-vong-bi.pdf">Lựa chọn mỡ bôi
                            trơn Vòng bi</a></h2>
                    <p></p>
                </div>
            </div>
            <div class="item">
                <div class="inner">
                    <a title="Tải xuống Tài liệu kiến thức về Bôi trơn Vòng bi" target="_blank"
                        href="/assets/catalogue/moboitron/boi-tron-vong-bi.pdf" class="btn btn-info"><i
                            class="fa fa-download"></i></a>

                    <h2 class="tt"><a title="Tải xuống Tài liệu kiến thức về Bôi trơn Vòng bi" target="_blank"
                            href="/assets/catalogue/moboitron/boi-tron-vong-bi.pdf">Tài liệu kiến thức về Bôi
                            trơn Vòng bi</a></h2>
                    <p></p>
                </div>
            </div>
            <div class="item">
                <div class="inner">
                    <a title="Tải xuống Tài liệu SKF SYSTEM 24 LAGD Series" target="_blank"
                        href="/assets/catalogue/moboitron/SKF-SYSTEM-24-LAGD-Series.pdf"
                        class="btn btn-info"><i class="fa fa-download"></i></a>

                    <h2 class="tt"><a title="Tải xuống Tài liệu SKF SYSTEM 24 LAGD Series" target="_blank"
                            href="/assets/catalogue/moboitron/SKF-SYSTEM-24-LAGD-Series.pdf">Tài liệu SKF
                            SYSTEM 24 LAGD Series</a></h2>
                    <p></p>
                </div>
            </div>
            <div class="item">
                <div class="inner">
                    <a title="Tải xuống Thông số kỹ thuật Mỡ SKF LGWA 2" target="_blank"
                        href="/assets/catalogue/moboitron/SKF-LGWA-2.pdf" class="btn btn-info"><i
                            class="fa fa-download"></i></a>

                    <h2 class="tt"><a title="Tải xuống Thông số kỹ thuật Mỡ SKF LGWA 2" target="_blank"
                            href="/assets/catalogue/moboitron/SKF-LGWA-2.pdf">Thông số kỹ thuật Mỡ SKF LGWA
                            2</a></h2>
                    <p></p>
                </div>
            </div>
            <div class="item">
                <div class="inner">
                    <a title="Tải xuống Thông số kỹ thuật Mỡ SKF LGMT 3" target="_blank"
                        href="/assets/catalogue/moboitron/SKF-LGMT-3.pdf" class="btn btn-info"><i
                            class="fa fa-download"></i></a>

                    <h2 class="tt"><a title="Tải xuống Thông số kỹ thuật Mỡ SKF LGMT 3" target="_blank"
                            href="/assets/catalogue/moboitron/SKF-LGMT-3.pdf">Thông số kỹ thuật Mỡ SKF LGMT
                            3</a></h2>
                    <p></p>
                </div>
            </div>
            <div class="item">
                <div class="inner">
                    <a title="Tải xuống Thông số kỹ thuật Mỡ SKF LGHB 2" target="_blank"
                        href="/assets/catalogue/moboitron/SKF-LGHB-2.pdf" class="btn btn-info"><i
                            class="fa fa-download"></i></a>

                    <h2 class="tt"><a title="Tải xuống Thông số kỹ thuật Mỡ SKF LGHB 2" target="_blank"
                            href="/assets/catalogue/moboitron/SKF-LGHB-2.pdf">Thông số kỹ thuật Mỡ SKF LGHB
                            2</a></h2>
                    <p></p>
                </div>
            </div>
            <div class="item">
                <div class="inner">
                    <a title="Tải xuống Thông số kỹ thuật Mỡ SKF LGHP 2" target="_blank"
                        href="/assets/catalogue/moboitron/SKF-LGHP2.pdf" class="btn btn-info"><i
                            class="fa fa-download"></i></a>

                    <h2 class="tt"><a title="Tải xuống Thông số kỹ thuật Mỡ SKF LGHP 2" target="_blank"
                            href="/assets/catalogue/moboitron/SKF-LGHP2.pdf">Thông số kỹ thuật Mỡ SKF LGHP
                            2</a></h2>
                    <p></p>
                </div>
            </div>
            <div class="item">
                <div class="inner">
                    <a title="Tải xuống Thông số kỹ thuật Mỡ SKF LGEP 2" target="_blank"
                        href="/assets/catalogue/moboitron/SKF-LGEP-2.pdf" class="btn btn-info"><i
                            class="fa fa-download"></i></a>

                    <h2 class="tt"><a title="Tải xuống Thông số kỹ thuật Mỡ SKF LGEP 2" target="_blank"
                            href="/assets/catalogue/moboitron/SKF-LGEP-2.pdf">Thông số kỹ thuật Mỡ SKF LGEP
                            2</a></h2>
                    <p></p>
                </div>
            </div>

            <div class="clearfix"></div>

        </div>
    </div>


</div>
@endsection
