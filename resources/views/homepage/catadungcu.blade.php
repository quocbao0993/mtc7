@extends('master')
@section('content')
    <div class="container">

        <h1 style="font-size:20px ;font-weight:700">Catalogue dụng cụ bảo trì SKF</h1>


        <br>
        <style>
            .download-list .item {
                float: left;
                width: 50%;
                padding: 15px;
                padding-right: 30px;
            }

            .download-list .item.col2 {
                padding-left: 30px;
                padding-right: 15px;
            }

            .download-list .item .inner {
                border-bottom: 1px dotted #ccc;
                position: relative;
                padding-right: 50px;
                min-height: 56px;
            }

            .download-list .item .inner .tt {
                padding: 0 0 5px;
                margin: 0;
                font-size: 16px;
                display: block;
                white-space: nowrap;
                overflow: hidden;
                text-overflow: ellipsis;
                font-weight: 400;
            }

            .download-list .item .inner .btn {
                position: absolute;
                right: 0;
            }

            .download-list .item .inner p {
                display: block;
                white-space: nowrap;
                overflow: hidden;
                text-overflow: ellipsis;
            }

            @media (max-width: 991px) {
                .download-list .item {
                    width: auto;
                    float: none;
                    padding-right: 15px;
                }

                .download-list .item.col2 {
                    padding-left: 15px;
                }
            }
        </style>

        <div style="margin-top: -50px">
            <div class="page-header"></div>
            <div class="download-list row">

                <div class="item">
                    <div class="inner">
                        <a title="Tải xuống Catalogue các loại máy gia nhiệt vòng bi SKF" target="_blank"
                            href="/assets/catalogue/dungcubaotri/may-gia-nhiet-vong-bi-skf.pdf" class="btn btn-info"><i
                                class="fa fa-download"></i></a>

                        <h2 class="tt"><a title="Tải xuống Catalogue các loại máy gia nhiệt vòng bi SKF" target="_blank"
                                href="/assets/catalogue/dungcubaotri/may-gia-nhiet-vong-bi-skf.pdf">Catalogue các loại
                                máy gia nhiệt vòng bi SKF</a></h2>
                        <p></p>
                    </div>
                </div>
                <div class="item">
                    <div class="inner">
                        <a title="Tải xuống Dụng cụ bảo trì SKF tiếng việt" target="_blank"
                            href="/assets/catalogue/dungcubaotri/dung-cu-bao-tri-skf-tv.pdf" class="btn btn-info"><i
                                class="fa fa-download"></i></a>

                        <h2 class="tt"><a title="Tải xuống Dụng cụ bảo trì SKF tiếng việt" target="_blank"
                                href="/assets/catalogue/dungcubaotri/dung-cu-bao-tri-skf-tv.pdf">Dụng cụ bảo trì SKF tiếng việt</a>
                        </h2>
                        <p></p>
                    </div>
                </div>
                <div class="item">
                    <div class="inner">
                        <a title="Tải xuống Dụng cụ kiểm tra tình trạng thiết bị" target="_blank"
                            href="/assets/catalogue/dungcubaotri/Dung_cu_kt_.tinhtrang.pdf" class="btn btn-info"><i
                                class="fa fa-download"></i></a>

                        <h2 class="tt"><a title="Tải xuống Dụng cụ kiểm tra tình trạng thiết bị" target="_blank"
                                href="/assets/catalogue/dungcubaotri/Dung_cu_kt_.tinhtrang.pdf">Dụng cụ kiểm tra tình trạng thiết
                                bị</a></h2>
                        <p></p>
                    </div>
                </div>
                <div class="item">
                    <div class="inner">
                        <a title="Tải xuống Tài liệu cảo SKF" target="_blank" href="/assets/catalogue/dungcubaotri/Cao_SKF_2014.pdf"
                            class="btn btn-info"><i class="fa fa-download"></i></a>

                        <h2 class="tt"><a title="Tải xuống Tài liệu cảo SKF" target="_blank"
                                href="/assets/catalogue/dungcubaotri/Cao_SKF_2014.pdf">Tài liệu cảo SKF</a></h2>
                        <p></p>
                    </div>
                </div>
                <div class="item">
                    <div class="inner">
                        <a title="Tải xuống Dùng đai ốc thủy lực tháo lắp vòng bi" target="_blank"
                            href="/assets/catalogue/dungcubaotri/Dung_dai_oc_thuy_luc_thao_lap_vong_bi.pdf" class="btn btn-info"><i
                                class="fa fa-download"></i></a>

                        <h2 class="tt"><a title="Tải xuống Dùng đai ốc thủy lực tháo lắp vòng bi" target="_blank"
                                href="/assets/catalogue/dungcubaotri/Dung_dai_oc_thuy_luc_thao_lap_vong_bi.pdf">Dùng đai ốc thủy lực
                                tháo lắp vòng bi</a></h2>
                        <p></p>
                    </div>
                </div>
                <div class="item">
                    <div class="inner">
                        <a title="Tải xuống Smart Tools" target="_blank" href="/assets/catalogue/dungcubaotri/Smart_Tools.pdf"
                            class="btn btn-info"><i class="fa fa-download"></i></a>

                        <h2 class="tt"><a title="Tải xuống Smart Tools" target="_blank"
                                href="/assets/catalogue/dungcubaotri/Smart_Tools.pdf">Smart Tools</a></h2>
                        <p></p>
                    </div>
                </div>
                <div class="item">
                    <div class="inner">
                        <a title="Tải xuống Máy căn chỉnh đồng tâm" target="_blank"
                            href="/assets/catalogue/dungcubaotri/May_can_chinh_dongtam.pdf" class="btn btn-info"><i
                                class="fa fa-download"></i></a>

                        <h2 class="tt"><a title="Tải xuống Máy căn chỉnh đồng tâm" target="_blank"
                                href="/assets/catalogue/dungcubaotri/May_can_chinh_dongtam.pdf">Máy căn chỉnh đồng tâm</a></h2>
                        <p></p>
                    </div>
                </div>
                <div class="item">
                    <div class="inner">
                        <a title="Tải xuống Súng bơm mỡ và phụ kiện" target="_blank"
                            href="/assets/catalogue/dungcubaotri/sung_bom_mo_va_phu_kien.pdf" class="btn btn-info"><i
                                class="fa fa-download"></i></a>

                        <h2 class="tt"><a title="Tải xuống Súng bơm mỡ và phụ kiện" target="_blank"
                                href="/assets/catalogue/dungcubaotri/sung_bom_mo_va_phu_kien.pdf">Súng bơm mỡ và phụ kiện</a></h2>
                        <p></p>
                    </div>
                </div>
                <div class="item">
                    <div class="inner">
                        <a title="Tải xuống Mapro Product Cataloge" target="_blank"
                            href="/assets/catalogue/dungcubaotri/Mapro_Product_Cataloge.pdf" class="btn btn-info"><i
                                class="fa fa-download"></i></a>

                        <h2 class="tt"><a title="Tải xuống Mapro Product Cataloge" target="_blank"
                                href="/assets/catalogue/dungcubaotri/Mapro_Product_Cataloge.pdf">Mapro Product Cataloge</a></h2>
                        <p></p>
                    </div>
                </div>
                <div class="item">
                    <div class="inner">
                        <a title="Tải xuống Tài liệu dụng cụ bảo trì SKF" target="_blank"
                            href="/assets/catalogue/dungcubaotri/Dung_cu_bao_tri.pdf" class="btn btn-info"><i
                                class="fa fa-download"></i></a>

                        <h2 class="tt"><a title="Tải xuống Tài liệu dụng cụ bảo trì SKF" target="_blank"
                                href="/assets/catalogue/dungcubaotri/Dung_cu_bao_tri.pdf">Tài liệu dụng cụ bảo trì SKF</a></h2>
                        <p></p>
                    </div>
                </div>
                <div class="item">
                    <div class="inner">
                        <a title="Tải xuống Tài liệu dụng cụ căn chỉnh SKF" target="_blank"
                            href="/assets/catalogue/dungcubaotri/Dung_cu_can_chinh.pdf" class="btn btn-info"><i
                                class="fa fa-download"></i></a>

                        <h2 class="tt"><a title="Tải xuống Tài liệu dụng cụ căn chỉnh SKF" target="_blank"
                                href="/assets/catalogue/dungcubaotri/Dung_cu_can_chinh.pdf">Tài liệu dụng cụ căn chỉnh SKF</a></h2>
                        <p></p>
                    </div>
                </div>
                <div class="item">
                    <div class="inner">
                        <a title="Tải xuống Tài liệu SKF TKTL 10" target="_blank"
                            href="/assets/catalogue/dungcubaotri/TKTL-30.pdf" class="btn btn-info"><i
                                class="fa fa-download"></i></a>

                        <h2 class="tt"><a title="Tải xuống Tài liệu SKF TKTL 10" target="_blank"
                                href="/assets/catalogue/dungcubaotri/TKTL-30.pdf">Tài liệu SKF TKTL 10</a></h2>
                        <p></p>
                    </div>
                </div>
                <div class="item">
                    <div class="inner">
                        <a title="Tải xuống Tài liệu dải sản phẩm SKF TKTL" target="_blank"
                            href="/assets/catalogue/dungcubaotri/TKTL.pdf" class="btn btn-info"><i
                                class="fa fa-download"></i></a>

                        <h2 class="tt"><a title="Tải xuống Tài liệu dải sản phẩm SKF TKTL" target="_blank"
                                href="/assets/catalogue/dungcubaotri/TKTL.pdf">Tài liệu dải sản phẩm SKF TKTL</a></h2>
                        <p></p>
                    </div>
                </div>
                <div class="item">
                    <div class="inner">
                        <a title="Tải xuống Tài liệu SKF TKTL 30" target="_blank"
                            href="/assets/catalogue/dungcubaotri/TKTL-30.pdf" class="btn btn-info"><i
                                class="fa fa-download"></i></a>

                        <h2 class="tt"><a title="Tải xuống Tài liệu SKF TKTL 30" target="_blank"
                                href="/assets/catalogue/dungcubaotri/TKTL-30.pdf">Tài liệu SKF TKTL 30</a></h2>
                        <p></p>
                    </div>
                </div>
                <div class="item">
                    <div class="inner">
                        <a title="Tải xuống Tài liệu thông số kỹ thuật máy gia nhiệt SKF TIH 220M" target="_blank"
                            href="/assets/catalogue/dungcubaotri/TIH220m.pdf" class="btn btn-info"><i
                                class="fa fa-download"></i></a>

                        <h2 class="tt"><a title="Tải xuống Tài liệu thông số kỹ thuật máy gia nhiệt SKF TIH 220M"
                                target="_blank" href="/assets/catalogue/dungcubaotri/TIH220m.pdf">Tài liệu thông số kỹ
                                thuật máy gia nhiệt SKF TIH 220M</a></h2>
                        <p></p>
                    </div>
                </div>
                <div class="item">
                    <div class="inner">
                        <a title="Tải xuống Tài liệu thông số kỹ thuật máy gia nhiệt SKF TIH 100M/230V" target="_blank"
                            href="/assets/catalogue/dungcubaotri/TIH100m.pdf" class="btn btn-info"><i
                                class="fa fa-download"></i></a>

                        <h2 class="tt"><a title="Tải xuống Tài liệu thông số kỹ thuật máy gia nhiệt SKF TIH 100M/230V"
                                target="_blank" href="/assets/catalogue/dungcubaotri/TIH100m.pdf">Tài liệu thông số kỹ
                                thuật máy gia nhiệt SKF TIH 100M/230V</a></h2>
                        <p></p>
                    </div>
                </div>
                <div class="item">
                    <div class="inner">
                        <a title="Tải xuống Tài liệu thông số kỹ thuật máy gia nhiệt SKF TIH 030m/230V" target="_blank"
                            href="/assets/catalogue/dungcubaotri/TIH030m.pdf" class="btn btn-info"><i
                                class="fa fa-download"></i></a>

                        <h2 class="tt"><a title="Tải xuống Tài liệu thông số kỹ thuật máy gia nhiệt SKF TIH 030m/230V"
                                target="_blank" href="/assets/catalogue/dungcubaotri/TIH030m.pdf">Tài liệu thông số kỹ
                                thuật máy gia nhiệt SKF TIH 030m/230V</a></h2>
                        <p></p>
                    </div>
                </div>

                <div class="clearfix"></div>

            </div>
        </div>


    </div>
@endsection
