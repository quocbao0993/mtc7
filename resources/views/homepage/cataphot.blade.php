@extends('master')
@section('content')
    <div class="container">

        <h1 style="font-size:20px ;font-weight:700">Catalogue Phớt SKF</h1>


        <br>
        <style>
            .download-list .item {
                float: left;
                width: 50%;
                padding: 15px;
                padding-right: 30px;
            }

            .download-list .item.col2 {
                padding-left: 30px;
                padding-right: 15px;
            }

            .download-list .item .inner {
                border-bottom: 1px dotted #ccc;
                position: relative;
                padding-right: 50px;
                min-height: 56px;
            }

            .download-list .item .inner .tt {
                padding: 0 0 5px;
                margin: 0;
                font-size: 16px;
                display: block;
                white-space: nowrap;
                overflow: hidden;
                text-overflow: ellipsis;
                font-weight: 400;
            }

            .download-list .item .inner .btn {
                position: absolute;
                right: 0;
            }

            .download-list .item .inner p {
                display: block;
                white-space: nowrap;
                overflow: hidden;
                text-overflow: ellipsis;
            }

            @media (max-width: 991px) {
                .download-list .item {
                    width: auto;
                    float: none;
                    padding-right: 15px;
                }

                .download-list .item.col2 {
                    padding-left: 15px;
                }
            }
        </style>

        <div style="margin-top: -50px">
            <div class="page-header"></div>
            <div class="download-list row">

                <div class="item">
                    <div class="inner">
                        <a title="Tải xuống Catalogue Phớt chặn SKF" target="_blank" href="/assets/catalogue/phot/phot-SKF.pdf"
                            class="btn btn-info"><i class="fa fa-download"></i></a>

                        <h2 class="tt"><a title="Tải xuống Catalogue Phớt chặn SKF" target="_blank"
                                href="/assets/catalogue/phot/phot-SKF.pdf">Catalogue Phớt chặn SKF</a></h2>
                    </div>
                </div>
                <div class="item">
                    <div class="inner">
                        <a title="Tải xuống Catalogue Phớt chắn dầu công nghiệp SKF" target="_blank"
                            href="/assets/catalogue/phot/phot-chan-dau-cong-nghiep-skf.pdf" class="btn btn-info"><i
                                class="fa fa-download"></i></a>

                        <h2 class="tt"><a title="Tải xuống Catalogue Phớt chắn dầu công nghiệp SKF" target="_blank"
                                href="/assets/catalogue/phot/phot-chan-dau-cong-nghiep-skf.pdf">Catalogue Phớt chắn dầu công
                                nghiệp SKF</a></h2>
                    </div>
                </div>

                <div class="clearfix"></div>

            </div>
        </div>


    </div>
@endsection
