@extends('master')
@section('content')

    {{-- <iframe src="/assets/Thu xac nhan dai ly uy quyen SKF 2022.PDF" width="700" height="800"></iframe> --}}
    <style>
        .container_iframe {
          position: relative;
          width: 100%;
          overflow: hidden;
          padding-top: 56.25%; /* 16:9 Aspect Ratio */
        }

        .responsive-iframe {
          position: absolute;
          top: 0;
          left: 0;
          bottom: 0;
          right: 0;
          width: 100%;
          height: 100%;
          border: none;
        }
        </style>
        <div class="col-sm-12 ">
            <h4 class="section-title-7"><span class="roboto-slab uppercase">Đại lý ủy quyền vòng bi SKF chính hãng</span></h4>
        </div>
        <h2 style="padding: 15px; color: #000; font-size: 20px; font-weight: 700;">MTC chuyên cung cấp vòng bi SKF chính hãng</h2>
        <p style="padding: 22px; margin: 10px"><span style="font-size:16px"><strong>Với phương châm phát triển bền vững,&nbsp;kèm theo&nbsp;đội ngũ nhân viên tâm huyết, năng động và chuyên nghiệp, lộ trình hợp tác rộng khắp.&nbsp;SKF MTC nỗ lực mang lại giá trị và sự thịnh vượng cho Khách hàng khi sử dụng các sản phẩm SKF chính hãng.</strong></span></p>
        <p style="color: #000; font-weight: 700; padding: 22px; margin: 10px"><span style="font-size: medium;">Chúng tôi tự hào là đại lý phân phối ủy quyền vòng bi và các sản phẩm SKF chính hãng tại Việt Nam.</span></p>


        <div class="container_iframe">
          <iframe class="responsive-iframe" src="/assets/Thu xac nhan dai ly uy quyen SKF 2022.PDF#zoom=50"></iframe>
        </div>

@endsection
