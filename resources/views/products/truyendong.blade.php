@extends('master')
@section('content')

<section class="sec-padding">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 ">
                <h4 class="section-title-7"><span class="roboto-slab uppercase">CÁC GIẢI PHÁP TRUYỀN ĐỘNG</span></h4>
            </div>
            <!--end title-->

            <div class="col-md-4 col-sm-6 bmargin">
                <div class="shop-product-holder">
                    <a href="">
                        <div class="image-holder">

                            <img src="/assets/products/truyendong/daydai.png"
                                alt=""  class="center" width="150" height="150">
                        </div>
                    </a>
                </div>
                <div class="clearfix"></div>
                <br>
                <h5 class="less-mar1 roboto-slab"><a href="">DÂY ĐAI</a>
                </h5>
                <p></p>
                <h5 class="text-red-4"></h5>
                <br>
                {{-- <a class="btn btn-red-4 btn-small" href="#"></a> --}}
            </div>
            <!--end item-->

            <div class="col-md-4 col-sm-6 bmargin">
                <div class="shop-product-holder">
                    <a href="">
                        <div class="image-holder">

                            <img src="/assets/products/truyendong/puli.png"
                                alt=""  class="center" width="150" height="150">
                        </div>
                    </a>
                </div>
                <div class="clearfix"></div>
                <br>
                <h5 class="less-mar1 roboto-slab"><a href="">PULI</a>
                </h5>
                <p> </p>
                <h5 class="text-red-4"></h5>
                <br>
                {{-- <a class="btn btn-red-4 btn-small" href="#"></a> --}}
            </div>
            <!--end item-->
            <div class="col-md-4 col-sm-6 bmargin">
                <div class="shop-product-holder">
                    <a href="">
                        <div class="image-holder">

                            <img src="/assets/products/truyendong/xich.png"
                                alt=""  class="center" width="150" height="150">
                        </div>
                    </a>
                </div>
                <div class="clearfix"></div>
                <br>
                <h5 class="less-mar1 roboto-slab"><a href="">XÍCH</a>
                </h5>
                <p></p>
                <h5 class="text-red-4"></h5>
                <br>
                {{-- <a class="btn btn-red-4 btn-small" href="#"></a> --}}
            </div>
            <!--end item-->

            <div class="col-md-4 col-sm-6 bmargin">
                <div class="shop-product-holder">
                    <a href="">
                        <div class="image-holder">

                            <img src="/assets/products/truyendong/diaxich.png"
                                alt=""  class="center" width="150" height="150">
                        </div>
                    </a>
                </div>
                <div class="clearfix"></div>
                <br>
                <h5 class="less-mar1 roboto-slab"><a href="">ĐĨA XÍCH</a>
                </h5>
                <p></p>
                <h5 class="text-red-4"></h5>
                <br>
                {{-- <a class="btn btn-red-4 btn-small" href="#"></a> --}}
            </div>
            <!--end item-->

            <div class="col-md-4 col-sm-6 bmargin">
                <div class="shop-product-holder">
                    <a href="">
                        <div class="image-holder">

                            <img src="/assets/products/truyendong/bactruot.png"
                                alt=""  class="center" width="150" height="150">
                        </div>
                    </a>
                </div>
                <div class="clearfix"></div>
                <br>
                <h5 class="less-mar1 roboto-slab"><a href="">BẠC TRƯỢT VÀ Moayo</a>
                </h5>
                <p></p>
                <h5 class="text-red-4"></h5>
                <br>
                {{-- <a class="btn btn-red-4 btn-small" href="#"></a> --}}
            </div>
            <!--end item-->

            <div class="col-md-4 col-sm-6 bmargin">
                <div class="shop-product-holder">
                    <a href="">
                        <div class="image-holder">

                            <img src="/assets/products/truyendong/khopnoi.png"
                                alt=""  class="center" width="150" height="150">
                        </div>
                    </a>
                </div>
                <div class="clearfix"></div>
                <br>
                <h5 class="less-mar1 roboto-slab"><a href="">KHỚP NỐI</a>
                </h5>
                <p></p>
                <h5 class="text-red-4"></h5>
                <br>
                {{-- <a class="btn btn-red-4 btn-small" href="#"></a> --}}
            </div>
            <!--end item-->
        </div>
    </div>
</section>
@endsection
