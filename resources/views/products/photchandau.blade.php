@extends('master')
@section('content')

<section class="sec-padding">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 ">
                <h4 class="section-title-7"><span class="roboto-slab uppercase">PHỚT CHẶN DẦU CÔNG NGHIỆP SKF CHÍNH HÃNG</span></h4>
            </div>
            <!--end title-->

            <div class="col-md-4 col-sm-6 bmargin">
                <div class="shop-product-holder">
                    <a href="/phot-truyen-dong">
                        <div class="image-holder">

                            <img src="/assets/products/photcongnghiep/phottruyendong.png"
                                alt=""  class="center" width="150" height="150">
                        </div>
                    </a>
                </div>
                <div class="clearfix"></div>
                <br>
                <h5 class="less-mar1 roboto-slab"><a href="/phot-truyen-dong">PHỚT TRUYỀN ĐỘNG</a>
                </h5>
                <p></p>
                <h5 class="text-red-4"></h5>
                <br>
                {{-- <a class="btn btn-red-4 btn-small" href="#"></a> --}}
            </div>
            <!--end item-->

            <div class="col-md-4 col-sm-6 bmargin">
                <div class="shop-product-holder">
                    <a href="/phot-thuy-luc">
                        <div class="image-holder">

                            <img src="/assets/products/photcongnghiep/photthuyluc.png"
                                alt=""  class="center" width="150" height="150">
                        </div>
                    </a>
                </div>
                <div class="clearfix"></div>
                <br>
                <h5 class="less-mar1 roboto-slab"><a href="/phot-thuy-luc">PHỚT THỦY LỰC</a>
                </h5>
                <p> </p>
                <h5 class="text-red-4"></h5>
                <br>
                {{-- <a class="btn btn-red-4 btn-small" href="#"></a> --}}
            </div>
            <!--end item-->

            <div class="col-md-4 col-sm-6 bmargin">
                <div class="shop-product-holder">
                    <a href="/phot-xu-ly-chat-long">
                        <div class="image-holder">

                            <img src="/assets/products/photcongnghiep/photchatlong.png"
                                alt=""  class="center" width="150" height="150">
                        </div>
                    </a>
                </div>
                <div class="clearfix"></div>
                <br>
                <h5 class="less-mar1 roboto-slab"><a href="/phot-xu-ly-chat-long">PHỚT CHẶN XỬ LÝ CHẤT LỎNG</a>
                </h5>
                <p></p>
                <h5 class="text-red-4"></h5>
                <br>
                {{-- <a class="btn btn-red-4 btn-small" href="#"></a> --}}
            </div>
            <!--end item-->

        </div>
    </div>
</section>
@endsection
