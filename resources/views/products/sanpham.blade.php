@extends('master')
@section('content')

<section class="sec-padding">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 ">
                <h4 class="section-title-7"><span class="roboto-slab uppercase">SẢN PHẨM SKF CÁC LOẠI CHÍNH HÃNG</span></h4>
            </div>
            <!--end title-->

            <div class="col-md-4 col-sm-6 bmargin">
                <div class="shop-product-holder">
                    <a href="/o-bi">
                        <div class="image-holder">
                            <img src="/assets/products/navbar/obi/obi.png"
                                alt="" class="center" width="150" height="150">
                        </div>
                    </a>
                </div>
                <div class="clearfix"></div>
                <br>
                <h5 class="less-mar1 roboto-slab"><a href="/o-bi">Ổ BI CÁC LOẠI SKF CHÍNH HÃNG</a>
                </h5>
                <p></p>
                <h5 class="text-red-4"></h5>
                <br>
                {{-- <a class="btn btn-red-4 btn-small" href="#"></a> --}}
            </div>
            <!--end item-->

            <div class="col-md-4 col-sm-6 bmargin">
                <div class="shop-product-holder">
                    <a href="/cum-goi-doi-o-lan">
                        <div class="image-holder">
                            <img src="/assets/products/navbar/goidovaolan/cumolan.png"
                                alt="" class="center" width="150" height="150">
                        </div>
                    </a>
                </div>
                <div class="clearfix"></div>
                <br>
                <h5 class="less-mar1 roboto-slab"><a href="">CỤM GỐI ĐỠ VÀ Ổ LĂN</a>
                </h5>
                <p> </p>
                <h5 class="text-red-4"></h5>
                <br>
                {{-- <a class="btn btn-red-4 btn-small" href="#"></a> --}}
            </div>
            <!--end item-->

            <div class="col-md-4 col-sm-6 bmargin">
                <div class="shop-product-holder">
                    <a href="/vong-bi-chinh-xac">
                        <div class="image-holder">
                            <img src="/assets/products/navbar/vongbisieuchinhxac/obitiepxucgoc.jfif"
                                alt="" class="center" width="150" height="150">
                        </div>
                    </a>
                </div>
                <div class="clearfix"></div>
                <br>
                <h5 class="less-mar1 roboto-slab"><a href="/vong-bi-chinh-xac">VÒNG BI SIÊU CHÍNH XÁC</a>
                </h5>
                <p></p>
                <h5 class="text-red-4"></h5>
                <br>
                {{-- <a class="btn btn-red-4 btn-small" href="#"></a> --}}
            </div>
            <!--end item-->

            <div class="col-md-4 col-sm-6 bmargin">
                <div class="shop-product-holder">
                    <a href="/o-vanh-xoay">
                        <div class="image-holder">
                            <img src="/assets/products/navbar/ovanhxoay/4diem.png"
                                alt="" class="center" width="150" height="150">
                        </div>
                    </a>
                </div>
                <div class="clearfix"></div>
                <br>
                <h5 class="less-mar1 roboto-slab"><a href="/o-vanh-xoay">Ổ VÀNH XOAY</a>
                </h5>
                <p></p>
                <h5 class="text-red-4"></h5>
                <br>
            </div>
            <!--end item-->

            <div class="col-md-4 col-sm-6 bmargin">
                <div class="shop-product-holder">
                    <a href="/o-truot">
                        <div class="image-holder">
                            <img src="/assets/products/navbar/otruot/otruottangtrong.png"
                                alt="" class="center" width="150" height="150">
                        </div>
                    </a>
                </div>
                <div class="clearfix"></div>
                <br>
                <h5 class="less-mar1 roboto-slab"><a href="/o-truot">Ổ TRƯỢT</a>
                </h5>
                <p> </p>
                <h5 class="text-red-4"></h5>
                <br>

            </div>
            <!--end item-->
            <div class="col-md-4 col-sm-6 bmargin">
                <div class="shop-product-holder">
                    <a href="">
                        <div class="image-holder">
                            <img src="/assets/products/navbar/cacloaisanphamkhac/vongbitutinh.png"
                                alt="" class="center" width="150" height="150">
                        </div>
                    </a>
                </div>
                <div class="clearfix"></div>
                <br>
                <h5 class="less-mar1 roboto-slab"><a href="">CÁC HỆ THỐNG VÀ VÒNG BI TỪ TÍNH</a>
                </h5>
                <p> </p>
                <h5 class="text-red-4"></h5>
                <br>

            </div>
            <div class="col-md-4 col-sm-6 bmargin">
                <div class="shop-product-holder">
                    <a href="/phot-chan-dau">
                        <div class="image-holder">
                            <img src="/assets/products/navbar/cacloaisanphamkhac/photdaucongnghiep.png"
                                alt="" class="center" width="150" height="150">
                        </div>
                    </a>
                </div>
                <div class="clearfix"></div>
                <br>
                <h5 class="less-mar1 roboto-slab"><a href="/phot-chan-dau">PHỚT CHẶN DẦU CHUYÊN NGHIỆP</a>
                </h5>
                <p> </p>
                <h5 class="text-red-4"></h5>
                <br>

            </div>
            <div class="col-md-4 col-sm-6 bmargin">
                <div class="shop-product-holder">
                    <a href="/quan-ly-boi-tron">
                        <div class="image-holder">
                            <img src="/assets/products/navbar/cacloaisanphamkhac/quanlyboitron.png"
                                alt="" class="center" width="150" height="150">
                        </div>
                    </a>
                </div>
                <div class="clearfix"></div>
                <br>
                <h5 class="less-mar1 roboto-slab"><a href="/quan-ly-boi-tron">QUẢN LÝ BÔI TRƠN</a>
                </h5>
                <p> </p>
                <h5 class="text-red-4"></h5>
                <br>

            </div>
            <div class="col-md-4 col-sm-6 bmargin">
                <div class="shop-product-holder">
                    <a href="/san-pham-bao-tri">
                        <div class="image-holder">
                            <img src="/assets/products/navbar/cacloaisanphamkhac/spbaotri.png"
                                alt="" class="center" width="150" height="150">
                        </div>
                    </a>
                </div>
                <div class="clearfix"></div>
                <br>
                <h5 class="less-mar1 roboto-slab"><a href="/san-pham-bao-tri">SẢN PHẨM BẢO TRÌ</a>
                </h5>
                <p> </p>
                <h5 class="text-red-4"></h5>
                <br>

            </div>
            <div class="col-md-4 col-sm-6 bmargin">
                <div class="shop-product-holder">
                    <a href="/giam-sat">
                        <div class="image-holder">
                            <img src="/assets/products/navbar/cacloaisanphamkhac/giamsat.png"
                                alt="" class="center" width="150" height="150">
                        </div>
                    </a>
                </div>
                <div class="clearfix"></div>
                <br>
                <h5 class="less-mar1 roboto-slab"><a href="/giam-sat">HỆ THỐNG GIÁM SÁT TÌNH TRẠNG HOẠT ĐỘNG</a>
                </h5>
                <p> </p>
                <h5 class="text-red-4"></h5>
                <br>

            </div>
            <div class="col-md-4 col-sm-6 bmargin">
                <div class="shop-product-holder">
                    <a href="/truyen-dong">
                        <div class="image-holder">
                            <img src="/assets/products/navbar/cacloaisanphamkhac/truyendong.png"
                                alt="" class="center" width="150" height="150">
                        </div>
                    </a>
                </div>
                <div class="clearfix"></div>
                <br>
                <h5 class="less-mar1 roboto-slab"><a href="/truyen-dong">CÁC GIẢI PHÁP TRUYỀN ĐỘNG</a>
                </h5>
                <p> </p>
                <h5 class="text-red-4"></h5>
                <br>

            </div>
            <div class="col-md-4 col-sm-6 bmargin">
                <div class="shop-product-holder">
                    <a href="/do-kiem-tra">
                        <div class="image-holder">
                            <img src="/assets/products/navbar/cacloaisanphamkhac/dovakiemtra.png"
                                alt="" class="center" width="150" height="150">
                        </div>
                    </a>
                </div>
                <div class="clearfix"></div>
                <br>
                <h5 class="less-mar1 roboto-slab"><a href="/do-kiem-tra">THIỆT BỊ ĐO VÀ KIỂM TRA</a>
                </h5>
                <p> </p>
                <h5 class="text-red-4"></h5>
                <br>

            </div>
        </div>
    </div>
</section>
@endsection
