@extends('master')
@section('content')

<section class="sec-padding">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 ">
                <h4 class="section-title-7"><span class="roboto-slab uppercase">THIẾT BỊ ĐO VÀ KIỂM TRA</span></h4>
            </div>
            <!--end title-->

            <div class="col-md-3 col-sm-6 bmargin">
                <div class="shop-product-holder">
                    <a href="">
                        <div class="image-holder">

                            <img src="/assets/products/kiemtra/gonsong.png"
                                alt=""  class="center" width="150" height="150">
                        </div>
                    </a>
                </div>
                <div class="clearfix"></div>
                <br>
                <h5 class="less-mar1 roboto-slab"><a href="">THIẾT BỊ PHÂN TÍCH ĐỘ GỢN SÓNG, ĐỘ TRÒN VÀ HÌNH DẠNG</a>
                </h5>
                <p></p>
                <h5 class="text-red-4"></h5>
                <br>
                {{-- <a class="btn btn-red-4 btn-small" href="#"></a> --}}
            </div>
            <!--end item-->

            <div class="col-md-3 col-sm-6 bmargin">
                <div class="shop-product-holder">
                    <a href="">
                        <div class="image-holder">

                            <img src="/assets/products/kiemtra/tiengon.png"
                                alt=""  class="center" width="150" height="150">
                        </div>
                    </a>
                </div>
                <div class="clearfix"></div>
                <br>
                <h5 class="less-mar1 roboto-slab"><a href="">THIẾT BỊ KIỂM TRA TIẾNG ỒN VÀ ĐỘ RUNG ĐỘNG</a>
                </h5>
                <p> </p>
                <h5 class="text-red-4"></h5>
                <br>
                {{-- <a class="btn btn-red-4 btn-small" href="#"></a> --}}
            </div>
            <!--end item-->

            <div class="col-md-3 col-sm-6 bmargin">
                <div class="shop-product-holder">
                    <a href="">
                        <div class="image-holder">

                            <img src="/assets/products/kiemtra/gadaumo.png"
                                alt=""  class="center" width="150" height="150">
                        </div>
                    </a>
                </div>
                <div class="clearfix"></div>
                <br>
                <h5 class="less-mar1 roboto-slab"><a href="">BỘ GÁ KIỂM TRA DẦU MỠ</a>
                </h5>
                <p></p>
                <h5 class="text-red-4"></h5>
                <br>
                {{-- <a class="btn btn-red-4 btn-small" href="#"></a> --}}
            </div>
            <!--end item-->
            <div class="col-md-3 col-sm-6 bmargin">
                <div class="shop-product-holder">
                    <a href="">
                        <div class="image-holder">

                            <img src="/assets/products/kiemtra/dichvuskf.png"
                                alt=""  class="center" width="150" height="150">
                        </div>
                    </a>
                </div>
                <div class="clearfix"></div>
                <br>
                <h5 class="less-mar1 roboto-slab"><a href="">DỊCH VỤ KIỂM TRA ĐÁNH GIÁ TỪ SKF</a>
                </h5>
                <p></p>
                <h5 class="text-red-4"></h5>
                <br>
                {{-- <a class="btn btn-red-4 btn-small" href="#"></a> --}}
            </div>
            <!--end item-->

        </div>
    </div>
</section>
@endsection
