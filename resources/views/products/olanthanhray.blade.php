@extends('master')
@section('content')

<section class="sec-padding">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 ">
                <h4 class="section-title-7"><span class="roboto-slab uppercase">Ổ LĂN TRÊN THANH RAY SKF CHÍNH HÃNG</span></h4>
            </div>
            <!--end title-->

            <div class="col-md-3 col-sm-6 bmargin">
                <div class="shop-product-holder">
                    <a href="">
                        <div class="image-holder">

                            <img src="/assets/products/conlanthanhray/conlancam.png"
                                alt=""  class="center" width="150" height="150">
                        </div>
                    </a>
                </div>
                <div class="clearfix"></div>
                <br>
                <h5 class="less-mar1 roboto-slab"><a href="">CON LĂN CAM</a>
                </h5>
                <p></p>
                <h5 class="text-red-4"></h5>
                <br>
                {{-- <a class="btn btn-red-4 btn-small" href="#"></a> --}}
            </div>
            <!--end item-->

            <div class="col-md-3 col-sm-6 bmargin">
                <div class="shop-product-holder">
                    <a href="">
                        <div class="image-holder">

                            <img src="/assets/products/conlanthanhray/conlando.png"
                                alt=""  class="center" width="150" height="150">
                        </div>
                    </a>
                </div>
                <div class="clearfix"></div>
                <br>
                <h5 class="less-mar1 roboto-slab"><a href="">CON LĂN ĐỠ</a>
                </h5>
                <p> </p>
                <h5 class="text-red-4"></h5>
                <br>
                {{-- <a class="btn btn-red-4 btn-small" href="#"></a> --}}
            </div>
            <!--end item-->

            <div class="col-md-3 col-sm-6 bmargin">
                <div class="shop-product-holder">
                    <a href="">
                        <div class="image-holder">

                            <img src="/assets/products/conlanthanhray/cumconlan.png"
                                alt=""  class="center" width="150" height="150">
                        </div>
                    </a>
                </div>
                <div class="clearfix"></div>
                <br>
                <h5 class="less-mar1 roboto-slab"><a href="">CỤM CON LĂN CAM</a>
                </h5>
                <p></p>
                <h5 class="text-red-4"></h5>
                <br>
                {{-- <a class="btn btn-red-4 btn-small" href="#"></a> --}}
            </div>
            <!--end item-->

        </div>
    </div>
</section>
@endsection
