@extends('master')
@section('content')

<section class="sec-padding">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 ">
                <h4 class="section-title-7"><span class="roboto-slab uppercase">Ổ BI SKF CÁC LOẠI CHÍNH HÃNG</span></h4>
            </div>
            <!--end title-->

            <div class="col-md-3 col-sm-6 bmargin">
                <div class="shop-product-holder">
                    <a href="/o-bi-con">
                        <div class="image-holder">

                            <img src="/assets/products/navbar/obi/obi.png"
                                alt="" class="center" width="150" height="150">
                        </div>
                    </a>
                </div>
                <div class="clearfix"></div>
                <br>
                <h5 class="less-mar1 roboto-slab"><a href="/o-bi-con">Ổ BI SKF CHÍNH HÃNG</a>
                </h5>
                <p></p>
                <h5 class="text-red-4"></h5>
                <br>
                {{-- <a class="btn btn-red-4 btn-small" href="#"></a> --}}
            </div>
            <!--end item-->

            <div class="col-md-3 col-sm-6 bmargin">
                <div class="shop-product-holder">
                    <a href="/o-lan-con">
                        <div class="image-holder">

                            <img src="/assets/products/navbar/obi/olan.png"
                                alt="" class="center" width="150" height="150">
                        </div>
                    </a>
                </div>
                <div class="clearfix"></div>
                <br>
                <h5 class="less-mar1 roboto-slab"><a href="/o-lan-con">Ổ LĂN SKF CHÍNH HÃNG</a>
                </h5>
                <p> </p>
                <h5 class="text-red-4"></h5>
                <br>
                {{-- <a class="btn btn-red-4 btn-small" href="#"></a> --}}
            </div>
            <!--end item-->

            <div class="col-md-3 col-sm-6 bmargin">
                <div class="shop-product-holder">
                    <a href="/pk-vong-bi">
                        <div class="image-holder">

                            <img src="/assets/products/navbar/obi/phukienvongbi.png"
                                alt=""class="center" width="150" height="150">
                        </div>
                    </a>
                </div>
                <div class="clearfix"></div>
                <br>
                <h5 class="less-mar1 roboto-slab"><a href="/pk-vong-bi">PHỤ KIỆN VÒNG BI</a>
                </h5>
                <p></p>
                <h5 class="text-red-4"></h5>
                <br>
                {{-- <a class="btn btn-red-4 btn-small" href="#"></a> --}}
            </div>
            <!--end item-->

            <div class="col-md-3 col-sm-6 bmargin">
                <div class="shop-product-holder">
                    <a href="/sp-chuyen-biet">
                        <div class="image-holder">

                            <img src="/assets/products/navbar/obi/pkchuyenbiet.png"
                                alt=""class="center" width="150" height="150">
                        </div>
                    </a>
                </div>
                <div class="clearfix"></div>
                <br>
                <h5 class="less-mar1 roboto-slab"><a href="/sp-chuyen-biet">SẢN PHẨM CHUYÊN BIỆT</a>
                </h5>
                <p></p>
                <h5 class="text-red-4"></h5>
                <br>
            </div>
            <!--end item-->

            <div class="col-md-3 col-sm-6 bmargin">
                <div class="shop-product-holder">
                    <a href="/o-lan-thanh-ray">
                        <div class="image-holder">

                            <img src="/assets/products/navbar/obi/olantrenthanhray.png"
                                alt=""class="center" width="150" height="150">
                        </div>
                    </a>
                </div>
                <div class="clearfix"></div>
                <br>
                <h5 class="less-mar1 roboto-slab"><a href="/o-lan-thanh-ray">Ổ LĂN TRÊN THANH RAY</a>
                </h5>
                <p> </p>
                <h5 class="text-red-4"></h5>
                <br>

            </div>
            <!--end item-->

        </div>
    </div>
</section>
@endsection
