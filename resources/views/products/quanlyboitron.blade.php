@extends('master')
@section('content')

<section class="sec-padding">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 ">
                <h4 class="section-title-7"><span class="roboto-slab uppercase">QUẢN LÝ BÔI TRƠN SKF CHÍNH HÃNG</span></h4>
            </div>
            <!--end title-->

            <div class="col-md-6 col-sm-6 bmargin">
                <div class="shop-product-holder">
                    <a href="">
                        <div class="image-holder">

                            <img src="/assets/products/recondoil.png"
                                alt=""  class="center" width="150" height="150">
                        </div>
                    </a>
                </div>
                <div class="clearfix"></div>
                <br>
                <h5 class="less-mar1 roboto-slab"><a href="">SKF RecondOil</a>
                </h5>
                <p></p>
                <h5 class="text-red-4"></h5>
                <br>
                {{-- <a class="btn btn-red-4 btn-small" href="#"></a> --}}
            </div>
            <!--end item-->

            <div class="col-md-6 col-sm-6 bmargin">
                <div class="shop-product-holder">
                    <a href="/san-pham-boi-tron">
                        <div class="image-holder">

                            <img src="/assets/products/navbar/cacloaisanphamkhac/quanlyboitron.png"
                                alt=""  class="center" width="150" height="150">
                        </div>
                    </a>
                </div>
                <div class="clearfix"></div>
                <br>
                <h5 class="less-mar1 roboto-slab"><a href="/san-pham-boi-tron">SẢN PHẨM BÔI TRƠN</a>
                </h5>
                <p> </p>
                <h5 class="text-red-4"></h5>
                <br>
                {{-- <a class="btn btn-red-4 btn-small" href="#"></a> --}}
            </div>
            <!--end item-->

        </div>
    </div>
</section>
@endsection
