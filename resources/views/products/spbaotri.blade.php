@extends('master')
@section('content')

<section class="sec-padding">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 ">
                <h4 class="section-title-7"><span class="roboto-slab uppercase">PHỤ KIỆN VÒNG BI SKF CHÍNH HÃNG</span></h4>
            </div>
            <!--end title-->

            <div class="col-md-3 col-sm-6 bmargin">
                <div class="shop-product-holder">
                    <a href="">
                        <div class="image-holder">

                            <img src="/assets/products/baotri/dcthuyluc.png"
                                alt=""  class="center" width="150" height="150">
                        </div>
                    </a>
                </div>
                <div class="clearfix"></div>
                <br>
                <h5 class="less-mar1 roboto-slab"><a href="">DỤNG CỤ THỦY LỰC</a>
                </h5>
                <p></p>
                <h5 class="text-red-4"></h5>
                <br>
                {{-- <a class="btn btn-red-4 btn-small" href="#"></a> --}}
            </div>
            <!--end item-->

            <div class="col-md-3 col-sm-6 bmargin">
                <div class="shop-product-holder">
                    <a href="">
                        <div class="image-holder">

                            <img src="/assets/products/baotri/dccokhi.png"
                                alt=""  class="center" width="150" height="150">
                        </div>
                    </a>
                </div>
                <div class="clearfix"></div>
                <br>
                <h5 class="less-mar1 roboto-slab"><a href="">DỤNG CỤ CƠ KHÍ</a>
                </h5>
                <p> </p>
                <h5 class="text-red-4"></h5>
                <br>
                {{-- <a class="btn btn-red-4 btn-small" href="#"></a> --}}
            </div>
            <!--end item-->

            <div class="col-md-3 col-sm-6 bmargin">
                <div class="shop-product-holder">
                    <a href="">
                        <div class="image-holder">

                            <img src="/assets/products/baotri/maygianhiet.png"
                                alt=""  class="center" width="150" height="150">
                        </div>
                    </a>
                </div>
                <div class="clearfix"></div>
                <br>
                <h5 class="less-mar1 roboto-slab"><a href="">MÁY GIA NHIỆT</a>
                </h5>
                <p></p>
                <h5 class="text-red-4"></h5>
                <br>
                {{-- <a class="btn btn-red-4 btn-small" href="#"></a> --}}
            </div>
            <!--end item-->
            <div class="col-md-3 col-sm-6 bmargin">
                <div class="shop-product-holder">
                    <a href="">
                        <div class="image-holder">

                            <img src="/assets/products/baotri/dccanchinh.png"
                                alt=""  class="center" width="150" height="150">
                        </div>
                    </a>
                </div>
                <div class="clearfix"></div>
                <br>
                <h5 class="less-mar1 roboto-slab"><a href="">DỤNG CỤ CÂN CHỈNH</a>
                </h5>
                <p></p>
                <h5 class="text-red-4"></h5>
                <br>
                {{-- <a class="btn btn-red-4 btn-small" href="#"></a> --}}
            </div>
            <!--end item-->

        </div>
    </div>
</section>
@endsection
