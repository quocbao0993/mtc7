@extends('master')
@section('content')
    <div class="news-view">

        <h1 style="color: #000; font-size: 22px; font-weight: 700;">SKF Authenticate, Phần mềm kiểm tra vòng bi SKF giả</h1>

        <!--   <div class="fb-like" data-href="https://ngocanh.com/tai-lieu-vong-bi/SKF-Authenticate.html" data-width="100" data-layout="button_count"
             data-action="like" data-size="small" data-show-faces="false" data-share="true"></div>
        <br> <br>-->

        <div class="overview">
            <p>Nếu bạn mua vòng bi hay các sản phẩm SKF từ 1 đơn vị phân phối không phải là Đại lý ủy quyền của SKF tại Việt
                Nam bạn có thể sử dụng ứng dụng SKF Authenticate để tự kiểm tra xem sản phẩm SKF mình mua có phải là sản
                phẩm chính hãng dựa vào các dữ liệu bạn cung cấp cho chuyên gia của SKF thông qua ứng dụng SKF Authenticate
                trên điện thoại của bạn.</p>

            <p><img alt="tránh xa hàng giả hàng fake kém chất lượng" style="height: 610px; width: 1000px; display: inline;"
                    class="lazyload" data-original="/public/cuploads/images/baiviet/vong-bi-skf-gia.jpg"
                    src="/public/cuploads/images/baiviet/vong-bi-skf-gia.jpg"></p>

            <blockquote>
                <p><strong><a href="/san-pham/vong-bi-skf" target="_blank">Mua vòng bi SKF chính hãng ở đâu đảm
                            bảo?</a></strong></p>
            </blockquote>

            <h2><strong>Hướng dẫn sử dụng ứng dụng SKF Authenticate</strong></h2>

            <h3><strong>Tải ứng dụng SKF Authenticate về Điện thoại của bạn</strong></h3>

            <p>SKF cung cấp ứng dụng trên 2 nền tảng hệ điều hành iOS (iphone, ipad) và Android cho các điện thoại chạy hệ
                điều hành Android. Từ điện thoại di động của bạn bạn có thể vào kho ứng dụng Appstore trên iphone hoặc
                CHPlay trên Android để tìm ứng dụng tên là&nbsp;SKF Authenticate. Hoặc bạn cũng có thể tải trực tiếp từ link
                dưới đây.</p>

            <p style="text-align:justify"><em>Tải ứng dụng&nbsp;SKF Authenticate cho các thiết bị chạy iOS</em></p>

            <p style="text-align:justify"><a href="https://apps.apple.com/vn/app/skf-authenticate/id987442973?l=vi"
                    rel="nofollow" target="_blank"><img alt="Kiểm tra vòng bi SKF giả trên iPhone, iPad"
                        style="height: 59px; width: 200px; display: inline;" class="lazyload"
                        data-original="https://ngocanh.com/public/cuploads/images/baiviet/ios.png"
                        src="https://ngocanh.com/public/cuploads/images/baiviet/ios.png"></a></p>

            <ul>
                <li>
                    <h4 style="text-align:justify"><a href="https://apps.apple.com/vn/app/skf-authenticate/id987442973?l=vi"
                            rel="nofollow" target="_blank">Phần mềm hỗ trợ&nbsp;Kiểm tra vòng bi SKF giả trên iPhone,
                            iPad</a></h4>
                </li>
            </ul>

            <h4 style="text-align:justify"><em>Tải ứng dụng&nbsp;SKF Authenticate cho các thiết bị chạy Android</em></h4>

            <p style="text-align:justify"><a href="http://bit.ly/2vF5u9h" rel="nofollow" target="_blank"><img
                        alt="Kiểm tra vòng bi SKF giả trên các thiết bị Android"
                        style="height: 59px; width: 200px; display: inline;" class="lazyload"
                        data-original="/public/cuploads/images/baiviet/android.png"
                        src="/public/cuploads/images/baiviet/android.png"></a></p>

            <ul>
                <li>
                    <h4 style="text-align:justify"><a href="http://bit.ly/2vF5u9h" rel="nofollow" target="_blank">Phần mềm
                            hỗ trợ&nbsp;Kiểm tra vòng bi SKF giả trên các thiết bị Android</a></h4>
                </li>
            </ul>

            <h4><strong>Các bước sử dụng ứng dụng&nbsp;SKF Authenticate</strong></h4>

            <p>Sau khi đã tải ứng dụng&nbsp;SKF Authenticate về điện thoại của bạn, bạn tiến hành các bước sau để sử dụng
                ứng dụng.</p>

            <h3><strong>Bước 1: Mở ứng dụng Camera trên điện thoại của bạn</strong></h3>

            <p>Từ điện thoại bạn mở ứng dụng chụp hình trên điện thoại của bạn để tiến hành chụp ảnh sản phẩm.</p>

            <h3 style="text-align:justify"><strong>Bước 2: Chụp hình ảnh sản phẩm SKF bạn cần kiểm tra</strong></h3>

            <p style="text-align:justify">Dùng Điện thoại của Bạn sử dụng chức năng Camera để chụp ảnh sản phẩm SKF mà Bạn
                có nghi ngờ là hàng giả. Ở bước chụp ảnh Bạn cố gắng chụp thật rõ nét mọi góc cạnh của bao bì, và sản phẩm.
                Càng chi tiết càng tốt, (nên chụp khoảng 5 đến 10 ảnh)</p>

            <p style="text-align:justify"><img
                    alt="Kiểm tra Vòng bi SKF giả, ứng dụng SKF Authenticate Chụp ảnh bao quát mặt trên của vỏ hộp vòng bi SKF."
                    class="img-responsive lazyload" style="height: 563px; width: 1000px; display: block;"
                    data-original="/public/cuploads/images/baiviet/anh-1.jpg"
                    src="/public/cuploads/images/baiviet/anh-1.jpg"><br>
                <em>Chụp ảnh bao quát mặt trên&nbsp;của vỏ hộp vòng bi SKF.</em>
            </p>

            <p style="text-align:justify"><img
                    alt="Kiểm tra Vòng bi SKF giả, ứng dụng SKF Authenticate Chụp mặt dưới của vỏ hộp vòng bi skf."
                    class="img-responsive lazyload" style="height: 563px; width: 1000px; display: block;"
                    data-original="/public/cuploads/images/baiviet/anh-2.jpg"
                    src="/public/cuploads/images/baiviet/anh-2.jpg"><br>
                <em>Chụp mặt dưới của vỏ hộp vòng bi skf.</em>
            </p>

            <p style="text-align:justify"><img
                    alt="chụp mặt sau của vỏ hộp vòng bi skf Kiểm tra Vòng bi SKF giả, ứng dụng SKF Authenticate"
                    class="img-responsive lazyload" style="height: 563px; width: 1000px; display: block;"
                    data-original="/public/cuploads/images/baiviet/anh-3.jpg"
                    src="/public/cuploads/images/baiviet/anh-3.jpg"><br>
                <em>chụp mặt sau của vỏ hộp vòng bi skf</em>
            </p>

            <p style="text-align:justify"><img
                    alt="chụp ảnh vòng bi bên trong vỏ hộp (toàn cảnh) Kiểm tra Vòng bi SKF giả, ứng dụng SKF Authenticate"
                    class="img-responsive lazyload" style="height: 563px; width: 1000px; display: block;"
                    data-original="/public/cuploads/images/baiviet/anh-4.jpg"
                    src="/public/cuploads/images/baiviet/anh-4.jpg"><br>
                <em>chụp ảnh vòng bi bên trong vỏ hộp (toàn cảnh)</em>
            </p>

            <p style="text-align:justify"><img
                    alt="chụp cận cảnh vòng bi càng chi tiết càng tốt. Kiểm tra Vòng bi SKF giả, ứng dụng SKF Authenticate"
                    class="img-responsive lazyload" style="height: 563px; width: 1000px; display: block;"
                    data-original="/public/cuploads/images/baiviet/anh-5.jpg"
                    src="/public/cuploads/images/baiviet/anh-5.jpg"><br>
                <em>chụp cận cảnh vòng bi càng chi tiết càng tốt.</em><br>
                &nbsp;
            </p>

            <h3 style="text-align:justify"><strong>Bước 3: Mở ứng dụng SKF Authenticate trên điện thoại của bạn</strong>
            </h3>

            <p style="text-align:justify">Sau khi đã chụp ảnh xong. Bạn mở ứng dụng&nbsp;SKF Authenticate lên và chọn vào
                menu Start</p>

            <p style="text-align:justify"><img alt="Kiểm tra Vòng bi SKF giả, ứng dụng SKF Authenticate"
                    class="img-responsive lazyload" style="height: 616px; width: 1000px; display: block;"
                    data-original="/public/cuploads/images/baiviet/anh-6.jpg"
                    src="/public/cuploads/images/baiviet/anh-6.jpg"></p>

            <h3 style="text-align:justify"><strong>Bước 4:&nbsp;Chọn Menu Use existing photos (Nếu chưa làm bước 2 thì có
                    thể chọn menu Take new photos)</strong></h3>

            <h3 style="text-align:justify"><strong>Bước 5: Chọn các ảnh đã chụp để gửi tới chuyên gia SKF</strong></h3>

            <p style="text-align:justify">Ứng dụng sẽ truy cập vào thư viện ảnh của Bạn trên điện thoại. Lúc này Bạn chọn
                các ảnh mà Bạn đã thực hiện ở Bước 2, tích chọn toàn bộ các ảnh đã chụp, sau đó chọn nút Select bên phải của
                ứng dụng.&nbsp;</p>

            <p style="text-align:justify">Ứng dụng sẽ trở lại thư viện ảnh, Bạn chọn nút Done ở góc phải của ứng dụng để kết
                thúc quá trình chọn ảnh. Sau đó chọn nút Next ở góc phải ứng dụng để chuyển sang bước tiếp theo.</p>

            <h3 style="text-align:justify"><strong>Bước 6: Điền thông tin của bạn</strong></h3>

            <p style="text-align:justify">Điền thông tin để gửi tới SKF.</p>

            <ul>
                <li style="text-align:justify">Tại ô&nbsp;<strong>Product supplier name:</strong>&nbsp;Bạn gõ tên công ty,
                    hoặc tên đơn vị&nbsp;mà bạn đã mua sản phẩm SKF đó.<br>
                    &nbsp;</li>
                <li style="text-align:justify">Tại ô&nbsp;<strong>Comment:</strong>&nbsp;Bạn gõ nội dung mà bạn muốn gửi tới
                    VD: Vui lòng kiểm tra sản phẩm SKF sau có phải chính hãng không? Nội dung tiếng anh để chung chung là
                    (Please check that the SKF product is genuine.)<br>
                    &nbsp;</li>
                <li style="text-align:justify">Tại phần&nbsp;<strong>Your contact infomation:</strong>&nbsp;Bạn vui lòng
                    điền thông tin của bạn để SKF có thể phản hồi tới bạn sau khi đã kiểm tra sản phẩm bạn gửi.<br>
                    &nbsp;</li>
                <li style="text-align:justify"><strong>Company:</strong>&nbsp;Điền tên Công ty của bạn ( Nếu là cá nhân có
                    thể điền User )<br>
                    &nbsp;</li>
                <li style="text-align:justify"><strong>Sender:&nbsp;</strong>Tên của Bạn<br>
                    &nbsp;</li>
                <li style="text-align:justify"><strong>Email:</strong>&nbsp;Điền email của Bạn, Email này SKF sẽ gửi lại
                    thông tin cho bạn sau khi đánh giá sản phẩm. Vì vậy hãy sử dụng email chính xác để có thể nhận được
                    thông tin của SKF phản hồi.<br>
                    &nbsp;</li>
                <li style="text-align:justify"><strong>Phone:</strong>&nbsp;Số ĐT của Bạn<br>
                    &nbsp;</li>
                <li style="text-align:justify"><strong>Country:</strong>&nbsp;Chọn Việt Nam</li>
            </ul>

            <p style="text-align:justify">Sau khi đã điền đủ thông tin, Bạn chọn nút&nbsp;<strong>SEND</strong>&nbsp;ở bên
                phải trên cùng của ứng dụng. Ứng dụng sẽ hỏi Bạn có muốn gửi thông tin đã khai báo tới SKF không?
                Chọn&nbsp;<strong>Yes</strong>.&nbsp;</p>

            <p style="text-align:justify">Quá trình kết thúc. Bạn sẽ nhận được 1 Email tự động của SKF thông báo đã tiếp
                nhận yêu cầu của Bạn và sẽ phản hồi lại thông tin sớm. Trong email đó có toàn bộ thông tin bạn đã khai báo ở
                trên kèm các hình ảnh bạn đã gửi cho SKF.</p>

            <p style="text-align:justify">Sau đó vài tiếng hoặc vài ngày bạn sẽ nhận được phản hồi đánh giá của SKF về sản
                phẩm bạn nghi ngờ là giả qua email bạn đã gửi. Thời gian phản hồi có thể nhanh hoặc chậm tùy vào điều kiện
                múi giờ và hình ảnh sản phẩm bạn gửi.</p>

            <div style="background:#eee;border:1px solid #ccc;padding:5px 10px;">Tìm hiểu thêm:<a
                    href="https://www.skf.com/vn/vi/news-and-media/events/2017-04-25%20SKF%20Brand%20protection.html"
                    rel="nofollow" target="_blank"> Khuyến cáo từ SKF về sản phẩm SKF giả</a></div>

            <h2 style="text-align:justify"><strong>Video hướng dẫn sử dụng ứng dụng SKF Authenticate</strong></h2>

            <p><iframe frameborder="0" height="400" width="100%" class="lazyload"
                    data-src="https://www.youtube.com/embed/p1DCMf9ebZs"
                    src="https://www.youtube.com/embed/p1DCMf9ebZs"></iframe></p>

            <h2 style="text-align:justify"><strong>Mua hàng từ&nbsp;Đại lý ủy quyền SKF&nbsp;để đảm bảo sản phẩm chính
                    hãng</strong></h2>

            <h3 style="text-align:justify"><strong>SKF Ngọc Anh - Đại lý ủy quyền <a href="/san-pham/vong-bi-skf"
                        id="Vòng bi SKF chính hãng" name="Vòng bi SKF chính hãng">Vòng bi SKF chính hãng</a></strong></h3>

            <p style="text-align:justify">HN:&nbsp;<strong>LK 01.10, Liền kề Tổ 9 Mỗ Lao, Hà Đông, Hà Nội</strong><br>
                Website:&nbsp;<a href="https://ngocanh.com" style="text-align: justify;">https://ngocanh.com</a>&nbsp;-
                Email:&nbsp;<a href="mailto:info@ngocanh.com" style="text-align: justify;">info@ngocanh.com</a><br>
                Tel: (024) 85 865 866 - Hotline:&nbsp;<strong>096 123 8558 - 0763 356 999 - 033 999 5999</strong><br>
                QN: <strong>D908 - Khu đô thị MonBay, Phường Hồng Hải, TP Hạ Long, Quảng Ninh</strong><br>
                Tel: (0203) 6 559 395 - Hotline:&nbsp;<strong>0974 63 1369 -&nbsp;0986 345 768</strong></p>

            <p style="text-align:justify"><u><strong>Các bài viết liên quan:</strong></u></p>

            <ul>
                <li style="text-align:justify">
                    <h4><a href="/kiem-tra-vong-bi-gia-skf.html" id="Vòng bi bạc đạn SKF giả, cách phân biệt và kiểm tra"
                            name="Vòng bi bạc đạn SKF giả, cách phân biệt và kiểm tra">Vòng bi bạc đạn SKF giả, cách phân
                            biệt và kiểm tra</a></h4>
                </li>
                <li style="text-align:justify">
                    <h4><a href="/tai-lieu-vong-bi/Chat-luong-vong-bi-SKF-chinh-hang.html"
                            id="Chất lượng vòng bi SKF chính hãng" name="Chất lượng vòng bi SKF chính hãng"
                            style="background-size: 16px;">Chất lượng vòng bi SKF chính hãng</a></h4>
                </li>
                <li style="text-align:justify">
                    <h4><a href="/tai-lieu-vong-bi/xuat-xu-vong-bi-skf.html" id="Xuất xứ vòng bi SKF chính hãng"
                            name="Xuất xứ vòng bi SKF chính hãng" style="background-size: 16px;">Xuất xứ vòng bi SKF chính
                            hãng</a></h4>
                </li>
                <li style="text-align:justify">
                    <h4><a href="/tai-lieu-vong-bi/Nhung-loi-ich-khi-mua-vong-bi-SKF-tu-Dai-ly-uy-quyen.html"
                            id="Những lợi ích khi mua vòng bi SKF từ Đại lý ủy quyền"
                            name="Những lợi ích khi mua vòng bi SKF từ Đại lý ủy quyền"
                            style="background-size: 16px;">Những lợi ích khi mua vòng bi SKF từ Đại lý ủy quyền</a></h4>
                </li>
                <li style="text-align:justify">
                    <h4><a href="/tai-lieu-vong-bi/vong-bi-bac-dan-gia-nhung-hau-qua-khon-luong-khi-su-dung.html"
                            id="Vòng bi bạc đạn giả những hậu quả khôn lường khi sử dụng"
                            name="Vòng bi bạc đạn giả những hậu quả khôn lường khi sử dụng"
                            style="background-size: 16px;">Vòng bi bạc đạn giả những hậu quả khôn lường khi sử dụng</a>
                    </h4>
                </li>
            </ul>

            <br>

            <div class="fb-like" data-href="https://ngocanh.com/tai-lieu-vong-bi/SKF-Authenticate.html" data-width="100"
                data-layout="button_count" data-action="like" data-size="small" data-show-faces="false"
                data-share="true"></div>


        </div>
    </div>
@endsection
